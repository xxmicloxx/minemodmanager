package com.xxmicloxx.modmanager.events;

import com.google.common.eventbus.EventBus;

/**
 * Created with IntelliJ IDEA.
 * User: ml
 * Date: 11.10.13
 * Time: 17:18
 */
public class EventManager {
    private static EventBus eventBus = new EventBus();

    public static void register(Object object) {
        eventBus.register(object);
    }

    public static void unregister(Object object) {
        eventBus.unregister(object);
    }

    public static void post(Object event) {
        eventBus.post(event);
    }
}

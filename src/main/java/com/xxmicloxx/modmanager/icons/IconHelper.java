package com.xxmicloxx.modmanager.icons;

import javax.swing.*;

/**
 * @author miclo
 */
public class IconHelper {

    public static ImageIcon loadIcon(String path) {
        return new ImageIcon(IconHelper.class.getResource(path));
    }
}

package com.xxmicloxx.modmanager.versions;

import com.google.gson.Gson;
import com.xxmicloxx.modmanager.ConfigManager;
import com.xxmicloxx.modmanager.events.EventManager;
import com.xxmicloxx.modmanager.events.versions.LocalVersionsChangedEvent;
import com.xxmicloxx.modmanager.gson.GsonSingleton;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ml
 * Date: 11.10.13
 * Time: 15:42
 */
public class LocalVersionManager {

    private static LocalVersionManager instance;

    public static LocalVersionManager getInstance() {
        if (instance == null) {
            instance = new LocalVersionManager();
            instance.init();
        }
        return instance;
    }

    private List<Version> versions = new ArrayList<Version>();

    public List<Version> getVersions() {
        return versions;
    }

    public Version getVersionById(String id) {
        for (Version v : versions) {
            if (v.getId().equals(id)) {
                return v;
            }
        }
        return null;
    }

    public void destroyVersion(Version v) {
        try {
            FileUtils.deleteDirectory(ConfigManager.getInstance().getVersionDir(v));
            versions.remove(v);
            EventManager.post(new LocalVersionsChangedEvent());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void init() {
        File versionsDir = ConfigManager.getInstance().getVersionsDir();
        versionsDir.mkdirs();
        Gson gson = GsonSingleton.getInstance();
        for (File f : versionsDir.listFiles()) {
            if (!f.isDirectory()) {
                continue;
            }
            File versionFile = new File(f, f.getName() + ".json");
            if (!versionFile.exists()) {
                continue;
            }
            try {
                FileReader fileReader = new FileReader(versionFile);
                versions.add(gson.fromJson(fileReader, Version.class));
                fileReader.close();
            } catch (Exception e) {
                System.err.print("Error while trying to parse " + versionFile.getAbsolutePath() + ": ");
                System.err.println(e);
            }
        }
    }
}

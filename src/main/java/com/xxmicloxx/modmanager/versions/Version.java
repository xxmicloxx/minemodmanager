package com.xxmicloxx.modmanager.versions;

import com.xxmicloxx.modmanager.ConfigManager;
import com.xxmicloxx.modmanager.OperatingSystem;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * @author miclo
 */
public class Version {

    private String id;
    private Date time;
    private Date releaseTime;
    private String type;
    private String minecraftArguments;
    private List<Library> libraries;
    private String mainClass;
    private List<Rule> rules;
    private String assets;

    public String getAssets() {
        return assets;
    }

    public String getId() {
        return id;
    }

    public Date getTime() {
        return time;
    }

    public Date getReleaseTime() {
        return releaseTime;
    }

    public String getType() {
        return type;
    }

    public String getMinecraftArguments() {
        return minecraftArguments;
    }

    public List<Library> getLibraries() {
        return libraries;
    }

    public String getMainClass() {
        return mainClass;
    }

    public List<Rule> getRules() {
        return rules;
    }

    public Collection<Library> getRelevantLibraries() {
        List<Library> result = new ArrayList<Library>();
        for (Library library : this.libraries) {
            if (library.appliesToCurrentEnvironment()) {
                result.add(library);
            }
        }
        return result;
    }

    public Collection<File> getClassPath(OperatingSystem os, File base) {
        Collection<Library> libraries = getRelevantLibraries();
        Collection<File> result = new ArrayList<File>();
        for (Library library : libraries) {
            if (library.getNatives() == null) {
                result.add(new File(base, "libraries/" + library.getArtifactPath()));
            }
        }
        return result;
    }

    public Collection<String> getExtractFiles(OperatingSystem os) {
        Collection<Library> libraries = getRelevantLibraries();
        Collection<String> result = new ArrayList<String>();
        for (Library library : libraries) {
            Map<OperatingSystem, String> natives = library.getNatives();
            if ((natives != null) && (natives.containsKey(os))) {
                result.add("libraries/" + library.getArtifactPath(natives.get(os)));
            }
        }
        return result;
    }

    public Set<String> getRequiredFiles(OperatingSystem os) {
        Set<String> neededFiles = new HashSet<String>();

        for (Library library : getRelevantLibraries()) {
            if (library.getNatives() != null) {
                String natives = library.getNatives().get(os);
                if (natives != null) neededFiles.add("libraries/" + library.getArtifactPath(natives));
            }
            else { neededFiles.add("libraries/" + library.getArtifactPath()); }

        }

        return neededFiles;
    }

    public Set<Downloadable> getRequiredDownloadables(OperatingSystem os, File targetDirectory) throws MalformedURLException {
        Set<Downloadable> neededFiles = new HashSet<Downloadable>();

        for (Library library : getRelevantLibraries()) {
            String file = null;

            if (library.getNatives() != null) {
                String natives = (String)library.getNatives().get(os);
                if (natives != null)
                    file = library.getArtifactPath(natives);
            }
            else {
                file = library.getArtifactPath();
            }

            if (file != null) {
                URL url = new URL(library.getDownloadUrl() + file);
                File local = new File(targetDirectory, "libraries/" + file);
                File temp = new File(ConfigManager.getInstance().getLibraryCache(), file);

                if ((!local.isFile()) || (library.hasNoCustomUrl())) {
                    neededFiles.add(new Downloadable(url, local, temp));
                }
            }
        }

        return neededFiles;
    }

    @Override
    public String toString() {
        return getId();
    }
}

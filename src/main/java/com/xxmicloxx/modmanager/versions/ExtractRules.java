package com.xxmicloxx.modmanager.versions;

import java.util.List;

/**
 * @author miclo
 */
public class ExtractRules {

    private List<String> exclude;

    public List<String> getExclude() {
        return exclude;
    }

    public boolean shouldExtract(String path) {
        if (this.exclude != null) {
            for (String rule : this.exclude) {
                if (path.startsWith(rule)) {
                    return false;
                }
            }
        }
        return true;
    }
}

package com.xxmicloxx.modmanager.versions.assets;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: ml
 * Date: 11.12.13
 * Time: 16:10
 */
public class AssetIndex {
    private Map<String, AssetObject> objects;
    private boolean virtual;

    public AssetIndex() {
        this.objects = new LinkedHashMap<String, AssetObject>();
    }

    public Map<String, AssetObject> getFileMap() {
        return objects;
    }

    public Set<AssetObject> getUniqueObjects() {
        return new HashSet<AssetObject>(this.objects.values());
    }

    public boolean isVirtual() {
        return virtual;
    }

    public class AssetObject {
        private String hash;
        private long size;

        public String getHash() {
            return hash;
        }

        public long getSize() {
            return size;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            AssetObject that = (AssetObject) o;

            if (size != that.size) return false;
            if (hash != null ? !hash.equals(that.hash) : that.hash != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = hash != null ? hash.hashCode() : 0;
            result = 31 * result + (int) (size ^ (size >>> 32));
            return result;
        }
    }
}

package com.xxmicloxx.modmanager.versions.assets;

import com.xxmicloxx.modmanager.versions.Downloadable;

import java.io.File;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: ml
 * Date: 11.12.13
 * Time: 16:24
 */
public class AssetDownloadable extends Downloadable {
    private String expectedHash;
    private long expectedFilesize;

    public AssetDownloadable(URL url, File local, String expectedHash, long expectedFilesize) {
        super(url, local, null);
        this.expectedFilesize = expectedFilesize;
        this.expectedHash = expectedHash;
    }

    public String getExpectedHash() {
        return expectedHash;
    }

    public long getExpectedFilesize() {
        return expectedFilesize;
    }
}

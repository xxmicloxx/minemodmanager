package com.xxmicloxx.modmanager.versions;

import org.apache.commons.io.IOUtils;

import java.io.*;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
* Created with IntelliJ IDEA.
* User: ml
* Date: 13.10.13
* Time: 14:46
*/
public class Downloadable {
    private URL url;
    private File local;
    private volatile float progress;
    private File temp;

    public Downloadable(URL url, File local, File temp) {
        this.url = url;
        this.local = local;
        this.temp = temp;
    }

    public File getTemp() {
        return temp;
    }

    public URL getUrl() {
        return url;
    }

    public File getLocal() {
        return local;
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }

    @Override
    public String toString() {
        return "Downloadable{" +
                "url=" + url +
                ", local=" + local +
                '}';
    }

    public static String getEtag(HttpURLConnection connection) {
        return getEtag(connection.getHeaderField("ETag"));
    }

    public static String getEtag(String etag) {
        if (etag == null)
            etag = "-";
        else if ((etag.startsWith("\"")) && (etag.endsWith("\"")))
        {
            etag = etag.substring(1, etag.length() - 1);
        }

        return etag;
    }

    public static String getMD5(File file) {
        DigestInputStream stream = null;
        try
        {
            stream = new DigestInputStream(new FileInputStream(file), MessageDigest.getInstance("MD5"));
            byte[] buffer = new byte[65536];

            int read = stream.read(buffer);
            while (read >= 1)
                read = stream.read(buffer);
        } catch (Exception ignored) {
            return null;
        } finally {
            try {
                stream.close();
            } catch (IOException ignored) {}
        }

        return String.format("%1$032x", new BigInteger(1, stream.getMessageDigest().digest()));
    }

    public static String getSHA(File file) {
        DigestInputStream stream = null;
        try
        {
            stream = new DigestInputStream(new FileInputStream(file), MessageDigest.getInstance("SHA"));
            byte[] buffer = new byte[65536];

            int read = stream.read(buffer);
            while (read >= 1)
                read = stream.read(buffer);
        } catch (Exception ignored) {
            return null;
        } finally {
            try {
                stream.close();
            } catch (IOException ignored) {}
        }

        return String.format("%1$032x", new BigInteger(1, stream.getMessageDigest().digest()));
    }

    public static String copyAndDigest(InputStream inputStream, OutputStream outputStream, String algorithm, int hashLength) throws IOException {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Missing Digest " + algorithm, e);
        }
        final byte[] buffer = new byte[65536];
        try {
            for (int read = inputStream.read(buffer); read >= 1; read = inputStream.read(buffer)) {
                digest.update(buffer, 0, read);
                outputStream.write(buffer, 0, read);
            }
        } finally {
            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(outputStream);
        }
        return String.format("%1$0" + hashLength + "x", new BigInteger(1, digest.digest()));
    }
}

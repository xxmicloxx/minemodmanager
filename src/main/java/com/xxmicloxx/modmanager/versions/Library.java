package com.xxmicloxx.modmanager.versions;

import com.xxmicloxx.modmanager.OperatingSystem;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrSubstitutor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author miclo
 */
public class Library {

    private static final String LIBRARY_DOWNLOAD_BASE = "https://s3.amazonaws.com/Minecraft.Download/libraries/";
    private static final StrSubstitutor SUBSTITUTOR;
    private String name;
    private List<Rule> rules;
    private ExtractRules extract;
    private String url;
    private Map<OperatingSystem, String> natives;

    static {
        HashMap<String, String> replaceMap = new HashMap<String, String>();
        boolean is64Bit = System.getProperty("os.arch").contains("64");
        replaceMap.put("arch", is64Bit ? "64" : "32");
        SUBSTITUTOR = new StrSubstitutor(replaceMap);
    }

    public String getName() {
        return name;
    }

    public List<Rule> getRules() {
        return rules;
    }

    public ExtractRules getExtract() {
        return extract;
    }

    public boolean appliesToCurrentEnvironment() {
        if (this.rules == null) {
            return true;
        }
        Rule.Action lastAction = Rule.Action.DISALLOW;
        for (Rule rule : this.rules) {
            Rule.Action action = rule.getAction();
            if (action != null && (rule.getOsRestriction() == null || rule.getOsRestriction().isCurrentOperatingSystem())) {
                lastAction = action;
            }
        }
        return lastAction == Rule.Action.ALLOW;
    }

    public String getArtifactBaseDir() {
        if (this.name == null) throw new IllegalStateException("Cannot get artifact dir of empty/blank artifact");
        String[] parts = this.name.split(":", 3);
        return String.format("%s/%s/%s", new Object[] { parts[0].replaceAll("\\.", "/"), parts[1], parts[2] });
    }

    public String getArtifactPath() {
        return getArtifactPath(null);
    }

    public String getArtifactPath(String classifier) {
        if (this.name == null) throw new IllegalStateException("Cannot get artifact path of empty/blank artifact");
        return String.format("%s/%s", new Object[] { getArtifactBaseDir(), getArtifactFilename(classifier) });
    }

    public String getArtifactFilename(String classifier) {
        if (this.name == null) throw new IllegalStateException("Cannot get artifact filename of empty/blank artifact");
        String[] parts = this.name.split(":", 3);
        String result = String.format("%s-%s%s.jar", parts[1], parts[2], StringUtils.isEmpty(classifier) ? "" : ("-" + classifier));
        return SUBSTITUTOR.replace(result);
    }

    public boolean hasNoCustomUrl() {
        return this.url == null;
    }

    public String getDownloadUrl() {
        if (this.url != null) {
            return this.url;
        }
        return "https://libraries.minecraft.net/";
    }

    public Map<OperatingSystem, String> getNatives() {
        return natives;
    }
}

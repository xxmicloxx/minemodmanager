package com.xxmicloxx.modmanager.versions;

import com.xxmicloxx.modmanager.OperatingSystem;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author miclo
 */
public class Rule {

    private Action action;
    private OSRestriction os;

    public Action getAction() {
        return action;
    }

    public OSRestriction getOsRestriction() {
        return os;
    }

    public class OSRestriction {
        private OperatingSystem name;
        private String version;

        public OperatingSystem getName() {
            return name;
        }

        public String getVersion() {
            return version;
        }

        public boolean isCurrentOperatingSystem() {
            if ((this.name != null) && (this.name != OperatingSystem.getCurrentPlatform())) {
                return false;
            }
            if (this.version != null) {
                try {
                    Pattern pattern = Pattern.compile(this.version);
                    Matcher matcher = pattern.matcher(System.getProperty("os.version"));
                    if (!matcher.matches()) return false;
                } catch (Throwable ignored) {}
            }
            return true;
        }
    }

    public static enum Action {
        ALLOW,
        DISALLOW
    }
}

package com.xxmicloxx.modmanager.versions;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.Gson;
import com.xxmicloxx.modmanager.gson.GsonSingleton;
import com.xxmicloxx.modmanager.network.HttpClientSingleton;
import com.xxmicloxx.modmanager.network.NetworkCache;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.concurrent.Callable;

/**
 * @author miclo
 */
public class RemoteVersionManager {

    public static ListenableFuture<Version[]> getVersions() {
        return HttpClientSingleton.executorService.submit(new Callable<Version[]>() {
            @Override
            public Version[] call() throws Exception {
                File f = NetworkCache.get("https://s3.amazonaws.com/Minecraft.Download/versions/versions.json").get();
                String contents = FileUtils.readFileToString(f);
                Gson gson = GsonSingleton.getInstance();
                VersionContainer container = gson.fromJson(contents, VersionContainer.class);
                return container.getVersions();
            }
        });
    }

    public static ListenableFuture<Version> getBetterVersion(final Version input) {
        return HttpClientSingleton.executorService.submit(new Callable<Version>() {
            @Override
            public Version call() throws Exception {
                File f = NetworkCache.get("https://s3.amazonaws.com/Minecraft.Download/versions/" + input.getId() + "/" + input.getId() + ".json").get();
                String contents = FileUtils.readFileToString(f);
                Gson gson = GsonSingleton.getInstance();
                return gson.fromJson(contents, Version.class);
            }
        });
    }
}

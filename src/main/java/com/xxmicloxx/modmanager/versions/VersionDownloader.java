package com.xxmicloxx.modmanager.versions;

import com.alee.extended.window.WebProgressDialog;
import com.alee.laf.optionpane.WebOptionPane;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.xxmicloxx.modmanager.ConfigManager;
import com.xxmicloxx.modmanager.MainWindow;
import com.xxmicloxx.modmanager.OperatingSystem;
import com.xxmicloxx.modmanager.dialogs.ChooseVersionDialog;
import com.xxmicloxx.modmanager.dialogs.LibraryDownloadDialog;
import com.xxmicloxx.modmanager.events.EventManager;
import com.xxmicloxx.modmanager.events.versions.LocalVersionsChangedEvent;
import com.xxmicloxx.modmanager.gson.GsonSingleton;
import com.xxmicloxx.modmanager.i18n.I18n;
import com.xxmicloxx.modmanager.instances.Instance;
import com.xxmicloxx.modmanager.instances.InstanceManager;
import com.xxmicloxx.modmanager.network.HttpClientSingleton;
import com.xxmicloxx.modmanager.network.LibraryDownloader;
import com.xxmicloxx.modmanager.versions.assets.AssetDownloadable;
import com.xxmicloxx.modmanager.versions.assets.AssetIndex;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import sun.security.krb5.Config;

import javax.net.ssl.HttpsURLConnection;
import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IntelliJ IDEA.
 * User: ml
 * Date: 11.10.13
 * Time: 19:11
 */
public class VersionDownloader {

    public static void start() {
        final WebProgressDialog progressDialog = new WebProgressDialog(MainWindow.getInstance().getWindow(), I18n.i18n().get("versions.download.title"));
        progressDialog.setIndeterminate(true);
        progressDialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        progressDialog.setShowProgressText(true);
        progressDialog.setShowCloseButton(false);
        progressDialog.setModal(true);
        progressDialog.setIndeterminate(true);
        loadVersionList(progressDialog);
        progressDialog.setVisible(true);
    }

    private static void downloadResources(final WebProgressDialog progressDialog, final Version version) {
        downloadResources(progressDialog, new Runnable() {
            @Override
            public void run() {

            }
        }, version);
    }

    public static void downloadResources(final WebProgressDialog progressDialog, final Runnable onDone, final Version version) {
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressText(I18n.i18n().get("versions.download.resources", new String[]{"{n}", "0", "{a}", "0"}));
        new Thread(new Runnable() {
            @Override
            public void run() {
                File targetDir = ConfigManager.getInstance().getResourcesDir();
                final Set<Downloadable> downloadables = getResourceFiles(targetDir, version);
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.setMaximum(downloadables.size());
                        progressDialog.setIndeterminate(false);
                        progressDialog.setProgressText(I18n.i18n().get("versions.download.resources", new String[]{"{n}", "0", "{a}", String.valueOf(downloadables.size())}));
                    }
                });
                final AtomicInteger currentIndex = new AtomicInteger();
                final AtomicInteger threadCount = new AtomicInteger();
                for (final Downloadable d : downloadables) {
                    final AssetDownloadable ad = (AssetDownloadable) d;
                    int threadCountInt = ConfigManager.getInstance().getParallelDownloads();
                    while (threadCountInt >= ConfigManager.getInstance().getParallelDownloads()) {
                        Thread.yield();
                        synchronized (threadCount) {
                            threadCountInt = threadCount.get();
                        }
                    }
                    synchronized (threadCount) {
                        threadCount.incrementAndGet();
                    }
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            int triesLeft = 3;
                            while (triesLeft > 0) {
                                try {
                                    File target = d.getLocal();
                                    if (target.isFile() && FileUtils.sizeOf(target) == ad.getExpectedFilesize()) {
                                        // local file equals remote file
                                        break;
                                    }
                                    target.getParentFile().mkdirs();
                                    target.createNewFile();
                                    HttpURLConnection connection = (HttpURLConnection) d.getUrl().openConnection();
                                    int status = connection.getResponseCode();
                                    if (status / 100 == 2) {
                                        InputStream inputStream = connection.getInputStream();
                                        FileOutputStream outputStream = new FileOutputStream(target);
                                        String hash = Downloadable.copyAndDigest(inputStream, outputStream, "SHA", 40);
                                        if (hash.equalsIgnoreCase(ad.getExpectedHash())) {
                                            // successful download
                                            break;
                                        }
                                        System.err.println("Hashes for resource did not match: " + ad.getExpectedHash());
                                    } else {
                                        System.out.println("Error while downloading resource " + ad.getExpectedHash() + " (" + status + "), using local file");
                                        break;
                                    }
                                } catch (IOException e) {
                                    if (d.getLocal().isFile()) {
                                        System.out.println("Error while downloading resource " + ad.getExpectedHash() + " (" + e.getClass().getSimpleName() + ": '" + e.getMessage() + "'), using local file");
                                        break;
                                    }
                                    System.err.println("Error while downloading resource " + ad.getExpectedHash() + " (" + e.getClass().getSimpleName() + ": '" + e.getMessage() + "')!\nTries left: " + String.valueOf(triesLeft-1));
                                }
                                triesLeft--;
                            }
                            if (triesLeft == 0) {
                                //TODO BIIIIG ERROR
                            }
                            synchronized (currentIndex) {
                                currentIndex.incrementAndGet();
                            }
                            EventQueue.invokeLater(new Runnable() {
                                @Override
                                public void run() {
                                    synchronized (currentIndex) {
                                        progressDialog.setProgressText(I18n.i18n().get("versions.download.resources", new String[]{"{n}", String.valueOf(currentIndex.get()), "{a}", String.valueOf(downloadables.size())}));
                                        progressDialog.setProgress(currentIndex.get());
                                    }
                                }
                            });
                            synchronized (threadCount) {
                                threadCount.decrementAndGet();
                            }
                        }
                    }, "Download thread #"+threadCount.get()).start();
                }
                int threadCountInt = ConfigManager.getInstance().getParallelDownloads();
                while (threadCountInt > 0) {
                    Thread.yield();
                    synchronized (threadCount) {
                        threadCountInt = threadCount.get();
                    }
                }
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        onDone.run();
                    }
                });
            }
        }, "Resources Download Thread").start();
        progressDialog.setVisible(true);
    }

    private static Set<Downloadable> getResourceFiles(File targetDir, Version version) {
        Set<Downloadable> results = new HashSet<Downloadable>();
        String resourceUrl = "http://resources.download.minecraft.net/";
        InputStream inputStream = null;
        File assets = ConfigManager.getInstance().getResourcesDir();
        File objectsFolder = new File(assets, "objects");
        File indexesFolder = new File(assets, "indexes");
        String indexName = version.getAssets();
        if (indexName == null) {
            indexName = "legacy";
        }
        File indexFile = new File(indexesFolder, indexName + ".json");
        try {
            URL indexUrl = new URL("https://s3.amazonaws.com/Minecraft.Download/indexes/" + indexName + ".json");
            inputStream = HttpClientSingleton.getConnection(indexUrl).getInputStream();
            String json = IOUtils.toString(inputStream);
            FileUtils.writeStringToFile(indexFile, json);
            AssetIndex index = GsonSingleton.getInstance().fromJson(json, AssetIndex.class);
            for (AssetIndex.AssetObject object : index.getUniqueObjects()) {
                String filename = object.getHash().substring(0, 2) + "/" + object.getHash();
                File file = new File(objectsFolder, filename);
                if (!file.isFile() || FileUtils.sizeOf(file) != object.getSize()) {
                    Downloadable downloadable = new AssetDownloadable(new URL(resourceUrl + filename), file, object.getHash(), object.getSize());
                    results.add(downloadable);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return results;
    }

    private static void loadVersionList(final WebProgressDialog progressDialog) {
        progressDialog.setProgressText(I18n.i18n().get("versions.download.retrieving"));
        Futures.addCallback(RemoteVersionManager.getVersions(), new FutureCallback<Version[]>() {
            @Override
            public void onSuccess(Version[] versions) {
                selectVersion(versions, progressDialog);
            }

            @Override
            public void onFailure(Throwable throwable) {
                WebOptionPane.showMessageDialog(MainWindow.getInstance().getWindow(), I18n.i18n().get("versions.download.errorlist") + ": " + throwable.getLocalizedMessage(), I18n.i18n().get("generic.error"), JOptionPane.ERROR_MESSAGE);
                progressDialog.dispose();
            }
        });
    }

    private static void selectVersion(Version[] versions, final WebProgressDialog progressDialog) {
        progressDialog.setProgressText(I18n.i18n().get("versions.download.waitinguser"));
        final ChooseVersionDialog dialog = new ChooseVersionDialog(versions, progressDialog);
        dialog.setVisible(true);
        Version selectedVersion = dialog.getSelectedVersion();
        if (selectedVersion == null) {
            progressDialog.dispose();
            return;
        }
        progressDialog.setProgressText(I18n.i18n().get("versions.download.retrievinginfo"));
        Futures.addCallback(RemoteVersionManager.getBetterVersion(selectedVersion), new FutureCallback<Version>() {
            @Override
            public void onSuccess(final Version version) {
                downloadResources(progressDialog, new Runnable() {
                    @Override
                    public void run() {
                        downloadLibraries(version, progressDialog);
                    }
                }, version);
            }

            @Override
            public void onFailure(Throwable throwable) {
                WebOptionPane.showMessageDialog(MainWindow.getInstance().getWindow(), I18n.i18n().get("versions.download.errorinformation") + ": " + throwable.getLocalizedMessage(), I18n.i18n().get("generic.error"), JOptionPane.ERROR_MESSAGE);
                progressDialog.dispose();
            }
        });
    }

    private static void downloadLibraries(final Version version, final WebProgressDialog progressDialog) {
        try {
            progressDialog.setProgressText(I18n.i18n().get("versions.download.librarydownload"));
            final File versionDir = ConfigManager.getInstance().getVersionDir(version);
            final File targetDir = new File(versionDir, "tempTarget");
            Set<Downloadable> downloadables = version.getRequiredDownloadables(OperatingSystem.getCurrentPlatform(), targetDir);
            new LibraryDownloadDialog(downloadables, progressDialog).addLibraryDownloadListener(new LibraryDownloadDialog.LibraryDownloadListener() {
                @Override
                public void onDownloadFinished() {
                    downloadMinecraft(versionDir, version, targetDir, progressDialog);
                }
            });
            new LibraryDownloader(downloadables);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private static void downloadMinecraft(final File versionDir, final Version version, final File targetDir, final WebProgressDialog progressDialog) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    progressDialog.setIndeterminate(false);
                    progressDialog.setMinimum(0);
                    progressDialog.setMaximum(100);
                    progressDialog.setProgressText(I18n.i18n().get("versions.download.minecraftdownload"));
                    File jarTarget = new File(targetDir, version.getId() + ".jar");
                    URL source = new URL("https://s3.amazonaws.com/Minecraft.Download/versions/" + version.getId() + "/" + version.getId() + ".jar");
                    HttpsURLConnection connection = HttpClientSingleton.getConnection(source);
                    jarTarget.createNewFile();
                    FileOutputStream fos = new FileOutputStream(jarTarget);
                    BufferedInputStream bis = new BufferedInputStream(connection.getInputStream());
                    final long contentLength = connection.getContentLength();
                    long myDownloadedLength = 0;
                    long lastRefresh = 0;

                    byte[] data = new byte[1024];
                    int count;
                    while ((count = bis.read(data, 0, 1024)) != -1) {
                        fos.write(data, 0, count);
                        myDownloadedLength += count;
                        if (lastRefresh < System.currentTimeMillis()-100) {
                            lastRefresh = System.currentTimeMillis();
                            final long finalMyDownloadedLength = myDownloadedLength;
                            EventQueue.invokeLater(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialog.setIndeterminate(false);
                                    progressDialog.setProgress((int) ((finalMyDownloadedLength / (double) contentLength)*100));
                                }
                            });
                        }
                    }
                    fos.flush();
                    fos.close();
                    bis.close();
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.setIndeterminate(true);
                            packVersion(versionDir, version, targetDir, progressDialog);
                        }
                    });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, "Minecraft Downloader Thread").start();
    }

    private static void packVersion(final File versionDir, final Version version, final File targetDir, final WebProgressDialog progressDialog) {
        progressDialog.setProgressText(I18n.i18n().get("versions.download.packing"));
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    File targetFile = new File(versionDir, version.getId() + ".zip");
                    targetFile.delete();
                    ZipFile targetZip = new ZipFile(targetFile);
                    ZipParameters parameters = new ZipParameters();
                    boolean first = true;
                    for (File f : targetDir.listFiles()) {
                        if (f.isDirectory()) {
                            if (!first) {
                                targetZip.addFolder(f, parameters);
                            } else {
                                targetZip.createZipFileFromFolder(f, parameters, false, 0);
                            }
                        } else {
                            if (!first) {
                                targetZip.addFile(f, parameters);
                            } else {
                                targetZip.createZipFile(f, parameters);
                            }
                        }
                        first = false;
                    }
                    FileUtils.deleteDirectory(targetDir);
                } catch (ZipException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        LocalVersionManager.getInstance().getVersions().add(version);
                        EventManager.post(new LocalVersionsChangedEvent());
                        progressDialog.dispose();
                    }
                });
            }
        }, "Packing thread").start();
    }
}

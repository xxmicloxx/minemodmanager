package com.xxmicloxx.modmanager.process;

import com.xxmicloxx.modmanager.MainWindow;
import com.xxmicloxx.modmanager.instances.Instance;
import com.xxmicloxx.modmanager.panels.tabs.InstanceWatcher;

import java.util.List;

public class JavaProcess
{
    private static final int MAX_SYSOUT_LINES = 5;
    private final List<String> commands;
    private final Process process;
    private final LimitedCapacityList<String> sysOutLines;
    private JavaProcessRunnable onExit;
    private ProcessMonitorThread monitor;
    private Instance instance;
    private InstanceWatcher instanceWatcher;

    public JavaProcess(final List<String> commands, final Process process, Instance instance) {
        this.sysOutLines = new LimitedCapacityList<String>(String.class, 5);
        this.monitor = new ProcessMonitorThread(this);
        this.commands = commands;
        this.process = process;
        this.monitor.start();
        this.instance = instance;
        instanceWatcher = new InstanceWatcher(this);
        MainWindow.getInstance().addTab(instanceWatcher);
    }

    public Process getRawProcess() {
        return this.process;
    }
    
    public List<String> getStartupCommands() {
        return this.commands;
    }
    
    public String getStartupCommand() {
        return this.process.toString();
    }
    
    public LimitedCapacityList<String> getSysOutLines() {
        return this.sysOutLines;
    }
    
    public boolean isRunning() {
        try {
            this.process.exitValue();
        }
        catch (IllegalThreadStateException ex) {
            return true;
        }
        return false;
    }
    
    public void setExitRunnable(final JavaProcessRunnable runnable) {
        this.onExit = runnable;
    }
    
    public void safeSetExitRunnable(final JavaProcessRunnable runnable) {
        this.setExitRunnable(runnable);
        if (!this.isRunning() && runnable != null) {
            runnable.onJavaProcessEnded(this, instance);
        }
    }
    
    public JavaProcessRunnable getExitRunnable() {
        return this.onExit;
    }
    
    public int getExitCode() {
        try {
            return this.process.exitValue();
        }
        catch (IllegalThreadStateException ex) {
            ex.fillInStackTrace();
            throw ex;
        }
    }
    
    public String toString() {
        return "JavaProcess[commands=" + this.commands + ", isRunning=" + this.isRunning() + "]";
    }
    
    public void stop() {
        this.process.destroy();
    }

    public Instance getInstance() {
        return instance;
    }

    public void setInstance(Instance instance) {
        this.instance = instance;
    }

    public void processClosed() {
        instanceWatcher.setProcessEnded(getExitCode());
        instance.setRunningInstances(instance.getRunningInstances());
    }

    public InstanceWatcher getInstanceWatcher() {
        return instanceWatcher;
    }
}

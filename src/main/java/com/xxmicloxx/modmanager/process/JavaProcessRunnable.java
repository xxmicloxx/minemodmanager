package com.xxmicloxx.modmanager.process;

import com.xxmicloxx.modmanager.instances.Instance;

public interface JavaProcessRunnable
{
    void onJavaProcessEnded(JavaProcess p0, Instance instance);
}

package com.xxmicloxx.modmanager.process;

import java.util.Arrays;
import java.util.Collection;
import java.io.IOException;
import java.util.ArrayList;

import com.xxmicloxx.modmanager.OperatingSystem;
import com.xxmicloxx.modmanager.instances.Instance;

import java.io.File;
import java.util.List;

public class JavaProcessLauncher
{
    private final String jvmPath;
    private final List<String> commands;
    private File directory;
    private Instance instance;
    
    public JavaProcessLauncher(String jvmPath, Instance instance, final String... commands) {
        super();
        if (jvmPath == null) {
            jvmPath = OperatingSystem.getCurrentPlatform().getJavaDir();
        }
        this.jvmPath = jvmPath;
        this.commands = new ArrayList<String>(commands.length);
        this.addCommands(commands);
        this.instance = instance;
    }
    
    public JavaProcess start() throws IOException {
        final List<String> full = this.getFullCommands();
        StringBuilder sb = new StringBuilder();
        for (String s : full) {
            if (sb.length() != 0) {
                sb.append(" ");
            }
            sb.append(s);
        }
        System.out.println("Starting '" + instance.getName() + "' with the following command line:");
        System.out.println(sb.toString());
        return new JavaProcess(full, new ProcessBuilder(full).directory(this.directory).redirectErrorStream(true).start(), instance);
    }

    public Instance getInstance() {
        return instance;
    }

    public void setInstance(Instance instance) {
        this.instance = instance;
    }

    public List<String> getFullCommands() {
        final List<String> result = new ArrayList<String>(this.commands);
        result.add(0, this.getJavaPath());
        return result;
    }
    
    public List<String> getCommands() {
        return this.commands;
    }
    
    public void addCommands(final String... commands) {
        this.commands.addAll(Arrays.asList(commands));
    }
    
    public void addSplitCommands(final String commands) {
        this.addCommands(commands.split(" "));
    }
    
    public JavaProcessLauncher getDirectory(final File directory) {
        this.directory = directory;
        return this;
    }
    
    public File getDirectory() {
        return this.directory;
    }
    
    protected String getJavaPath() {
        return this.jvmPath;
    }
    
    public String toString() {
        return "JavaProcessLauncher[commands=" + this.commands + ", java=" + this.jvmPath + "]";
    }
}

package com.xxmicloxx.modmanager.process;

import com.xxmicloxx.modmanager.instances.Instance;
import org.apache.commons.io.IOUtils;

import java.awt.*;
import java.io.IOException;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProcessMonitorThread extends Thread
{
    private static final Logger LOGGER;
    private final JavaProcess process;

    public ProcessMonitorThread(final JavaProcess process) {
        super();
        this.process = process;
    }
    
    public void run() {
        final InputStreamReader reader = new InputStreamReader(this.process.getRawProcess().getInputStream());
        final BufferedReader buf = new BufferedReader(reader);
        String line;
        while (this.process.isRunning()) {
            try {
                while ((line = buf.readLine()) != null) {
                    System.out.println(process.getInstance().getName() + "> " + line);
                    this.process.getSysOutLines().add(line);
                    final String finalLine = line;
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            process.getInstanceWatcher().appendLogLine("Minecraft> " + finalLine);
                        }
                    });
                }
            }
            catch (IOException ex) {
                if (ex.getMessage().equals("Stream closed")) {
                    ProcessMonitorThread.LOGGER.log(Level.INFO, "Logging stream to '" + process.getInstance().getName() + "' closed.");
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    ProcessMonitorThread.LOGGER.log(Level.SEVERE, "Logging error: ", ex);
                }
            }
            finally {
                IOUtils.closeQuietly(reader);
            }
        }
        process.processClosed();
        final JavaProcessRunnable onExit = this.process.getExitRunnable();
        if (onExit != null) {
            onExit.onJavaProcessEnded(this.process, process.getInstance());
        }
    }
    
    static {
        LOGGER = Logger.getLogger("Minceraft");
    }
}

package com.xxmicloxx.modmanager.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: ml
 * Date: 10.10.13
 * Time: 20:13
 */
public class GsonSingleton {
    private static GsonBuilder instance;

    public static Gson getInstance() {
        if (instance == null) {
            instance = new GsonBuilder();
            instance.registerTypeAdapterFactory(new LowerCaseEnumTypeAdapterFactory());
            instance.registerTypeAdapter(Date.class, new DateTypeAdapter());
            instance.enableComplexMapKeySerialization();
            instance.setPrettyPrinting();
        }
        return instance.create();
    }
}

package com.xxmicloxx.modmanager;

import com.alee.extended.transition.ComponentTransition;
import com.alee.extended.transition.TransitionAdapter;
import com.alee.extended.transition.effects.Direction;
import com.alee.extended.transition.effects.curtain.CurtainTransitionEffect;
import com.alee.extended.transition.effects.curtain.CurtainType;
import com.alee.extended.transition.effects.fade.FadeTransitionEffect;
import com.alee.laf.StyleConstants;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.menu.WebPopupMenu;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.tabbedpane.WebTabbedPane;
import com.alee.utils.SwingUtils;
import com.alee.utils.swing.AncestorAdapter;
import com.xxmicloxx.modmanager.i18n.I18n;
import com.xxmicloxx.modmanager.panels.LoadablePanel;
import com.xxmicloxx.modmanager.panels.SplashPanel;
import com.xxmicloxx.modmanager.panels.tabs.*;

import javax.swing.*;
import javax.swing.event.AncestorEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;

/**
 * @author miclo
 * Represents the main window
 */
public class MainWindow {

    private JPanel contentPane;
    private WebTabbedPane tabbedPane1;
    private JTextField textField1;
    private JButton searchButton;
    private JButton btnUserDropdown;
    private JLabel helloLabel;
    private ArrayList<ITab> tabs = new ArrayList<ITab>();
    private JFrame window;
    private WebPopupMenu userMenu;

    public static final ImageIcon REMOVE_ICON = new ImageIcon(MainWindow.class.getResource("/com/xxmicloxx/modmanager/icons/remove.png"));
    private static MainWindow instance;

    public static MainWindow getInstance() {
        if (instance == null) {
            instance = new MainWindow();
        }
        return instance;
    }

    private MainWindow() {}

    public JFrame getWindow() {
        return window;
    }

    public void show() {
        searchButton.setText(I18n.i18n().get("generic.search"));
        helloLabel.setText(I18n.i18n().get("manager.greet", new String[]{"{account}", "Anonymous"}));
        final JPanel splashPane = new SplashPanel().mainPanel;
        window = new WebFrame("MineModManager");
        window.setLayout(new BorderLayout());
        window.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        final ComponentTransition transition = new ComponentTransition(splashPane) {
            @Override
            public Dimension getPreferredSize() {
                return contentPane.getPreferredSize();
            }
        };
        userMenu = new WebPopupMenu();
        JMenuItem item = new JMenuItem(I18n.i18n().get("accounts.login"));
        JMenuItem item2 = new JMenuItem(I18n.i18n().get("config.settings"));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LoginTab tab = new LoginTab();
                ITab foundTab;
                if ((foundTab = getTab(tab.getClass())) != null) {
                    ((LoginTab) foundTab).setCurrentPanel(new LoginTabLogin().mainPanel);
                    showTab(foundTab);
                } else {
                    addTab(tab, true);
                }
            }
        });
        item2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SettingsTab tab = new SettingsTab();
                ITab foundTab;
                if ((foundTab = getTab(tab.getClass())) != null) {
                    ((SettingsTab) foundTab).setCurrentPanel(new SettingsTabSettings().mainPanel);
                    showTab(foundTab);
                } else {
                    addTab(tab, true);
                }
            }
        });
        userMenu.add(item);
        userMenu.add(item2);
        ImageIcon icon = new ImageIcon(MainWindow.class.getResource("/com/xxmicloxx/modmanager/icons/black-triangle.png"));
        Image img = icon.getImage();
        img = img.getScaledInstance(10, 10, Image.SCALE_SMOOTH);
        icon = new ImageIcon(img);
        btnUserDropdown.setIcon(icon);
        btnUserDropdown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userMenu.show(btnUserDropdown, (int) (btnUserDropdown.getPreferredSize().getWidth()/(-2)), btnUserDropdown.getHeight());
            }
        });
        FadeTransitionEffect effect = new FadeTransitionEffect();
        transition.setTransitionEffect(effect);
        transition.addAncestorListener(new AncestorAdapter() {
            @Override
            public void ancestorAdded(AncestorEvent ancestorEvent) {
                transition.delayTransition(1000, contentPane);
            }
        });
        transition.addTransitionListener(new TransitionAdapter() {
            @Override
            public void transitionFinished() {}
        });
        window.add(transition, BorderLayout.CENTER);
        window.pack();
        window.setLocationRelativeTo(null);
        window.setVisible(true);
        window.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent windowEvent) {
                stop();
            }
        });
        tabbedPane1.removeAll();
        InstancesTab iTab = new InstancesTab();
        LocalVersionsTab lvTab = new LocalVersionsTab();
        ModsTab modsTab = new ModsTab();
        LogTab logTab = new LogTab();
        addTab(iTab);
        addTab(lvTab);
        addTab(modsTab);
        addTab(logTab);
        iTab.applyI18n();
        showTab(iTab);
    }

    private void stop() {
        System.exit(0);
    }

    public void showTab(ITab tab) {
        for (Component c : tabbedPane1.getComponents()) {
            if (!(c instanceof LoadablePanel)) {
                continue;
            }
            LoadablePanel lp = (LoadablePanel) c;
            if (!tab.equals(lp.getTab())) {
                continue;
            }
            tabbedPane1.setSelectedComponent(c);
            return;
        }
    }

    public ITab getTab(Class<? extends ITab> tab) {
        for (Component c : tabbedPane1.getComponents()) {
            if (!(c instanceof LoadablePanel)) {
                continue;
            }
            LoadablePanel lp = (LoadablePanel) c;
            if (tab.isInstance(lp.getTab())) {
                return lp.getTab();
            }
        }
        return null;
    }

    public void addTab(ITab tab) {
        addTab(tab, false);
    }

    public void addTab(ITab tab, boolean closeable) {
        tabs.add(tab);
        LoadablePanel panel = new LoadablePanel();
        panel.setTab(tab);
        tabbedPane1.addTab(tab.getTabTitle(), panel);
        if (closeable) {
            tabbedPane1.setTabComponentAt(tabbedPane1.getTabCount() - 1, createRemovableTile(tab));
        }
    }

    public void setTabCloseable(ITab tab, boolean closeable) {
        for (int i = 0; i < tabbedPane1.getTabCount(); i++) {
            Component c = tabbedPane1.getComponentAt(i);
            if (!(c instanceof LoadablePanel)) {
                continue;
            }
            LoadablePanel lp = (LoadablePanel) c;
            if (tab.equals(lp.getTab())) {
                if (closeable) {
                    tabbedPane1.setTabComponentAt(i, createRemovableTile(tab));
                } else {
                    tabbedPane1.setTabComponentAt(i, null);
                }
                return;
            }
        }
    }

    public void closeTab(ITab tab) {
        for (Component c : tabbedPane1.getComponents()) {
            if (!(c instanceof LoadablePanel)) {
                continue;
            }
            LoadablePanel lp = (LoadablePanel) c;
            if (tab.equals(lp.getTab())) {
                tab.closing();
                tabbedPane1.remove(lp);
                return;
            }
        }
    }

    private Component createRemovableTile(final ITab tab)  {
        WebPanel removableTitle = new WebPanel();
        removableTitle.setOpaque(false);
        WebLabel titleLabel = new WebLabel(tab.getTabTitle());
        titleLabel.setMargin(0, 2, 0, 4);
        removableTitle.add(titleLabel, BorderLayout.CENTER);
        WebButton remove = WebButton.createIconWebButton(REMOVE_ICON, StyleConstants.smallRound, true);
        remove.setFocusable(false);
        remove.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                closeTab(tab);
            }
        });
        removableTitle.add(remove, BorderLayout.LINE_END);
        SwingUtils.copyOrientation(tabbedPane1, removableTitle);
        return removableTitle;
    }

    public void setLoading(ITab tab, boolean loading) {
        setLoading(tab, loading, new Runnable() {
            @Override
            public void run() {}
        });
    }

    public void setLoading(ITab tab, boolean loading, Runnable r) {
        for (Component c : tabbedPane1.getComponents()) {
            if (!(c instanceof LoadablePanel)) {
                continue;
            }
            LoadablePanel lp = (LoadablePanel) c;
            if (!lp.getTab().equals(tab)) {
                continue;
            }
            //same
            lp.setLoading(loading, r);
        }
    }

    public void refreshContent(ITab tab) {
        for (Component c : tabbedPane1.getComponents()) {
            if (!(c instanceof LoadablePanel)) {
                continue;
            }
            LoadablePanel lp = (LoadablePanel) c;
            if (!lp.getTab().equals(tab)) {
                continue;
            }
            // will refresh root pane
            lp.setTab(tab);
        }
    }

    public void restartApplication() throws Exception {
        final String javaBin = OperatingSystem.getCurrentPlatform().getJavaDir();
        final File currentJar = new File(MainWindow.class.getProtectionDomain().getCodeSource().getLocation().toURI());
        if(!currentJar.getName().endsWith(".jar")) {
            JOptionPane.showMessageDialog(null, "Error - not in jar mode.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        final ArrayList<String> command = new ArrayList<String>();
        command.add(javaBin);
        command.add("-jar");
        command.add(currentJar.getPath());
        final ProcessBuilder builder = new ProcessBuilder(command);
        builder.start();
        System.exit(0);
    }
}

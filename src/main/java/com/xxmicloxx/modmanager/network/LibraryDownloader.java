package com.xxmicloxx.modmanager.network;

import com.xxmicloxx.modmanager.versions.Downloadable;
import org.apache.commons.io.FileUtils;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.util.Collections;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: ml
 * Date: 11.10.13
 * Time: 22:55
 */
public class LibraryDownloader {
    private Set<Downloadable> downloadables;

    public LibraryDownloader(Set<Downloadable> downloadables) {
        this.downloadables = downloadables;
        startDownload();
    }

    public Set<Downloadable> getDownloadables() {
        return Collections.unmodifiableSet(downloadables);
    }

    private void startDownload() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (Downloadable d : downloadables) {
                    URL url = d.getUrl();
                    HttpsURLConnection urlConn = HttpClientSingleton.getConnection(url);
                    urlConn.setDoInput(true);

                    File outputFile = d.getLocal();
                    outputFile.getParentFile().mkdirs();
                    try {
                        outputFile.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    File tempFile = d.getTemp();
                    tempFile.getParentFile().mkdirs();
                    if (!tempFile.exists()) {
                        try {
                            tempFile.createNewFile();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        FileOutputStream fos = null;
                        try {
                            fos = new FileOutputStream(tempFile);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        BufferedInputStream bis = null;
                        try {
                            bis = new BufferedInputStream(urlConn.getInputStream());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        long contentLength = urlConn.getContentLengthLong();
                        long myDownloadedLength = 0;

                        byte[] data = new byte[1024];
                        int count;
                        try {
                            while ((count = bis.read(data, 0, 1024)) != -1) {
                                fos.write(data, 0, count);
                                myDownloadedLength += count;
                                d.setProgress((float) ((myDownloadedLength/(double)contentLength)*100));
                            }
                            fos.flush();
                            fos.close();
                            bis.close();
                        } catch (IOException e) {
                            //TODO error dialog
                            e.printStackTrace();
                        }
                    }
                    try {
                        FileUtils.copyFile(tempFile, outputFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    d.setProgress(100);
                }
            }
        }, "Library Download Thread").start();
    }
}

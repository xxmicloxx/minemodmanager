package com.xxmicloxx.modmanager.network;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.concurrent.Executors;

/**
 * @author miclo
 */
public class HttpClientSingleton {

    public static String HELPER_URL = "https://xxmicloxx.tk/MineModManager/parsehelper.php";
    public static ListeningExecutorService executorService = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());
    private static SSLSocketFactory sslFactory;

    public static String readAllLines(InputStream stream) {
        Reader r = new InputStreamReader(stream);
        StringBuilder sb = new StringBuilder();
        char[] chars = new char[4 * 1024];
        int len;
        try {
            while ((len = r.read(chars)) >= 0) {
                sb.append(chars, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public static HttpsURLConnection getConnection(URL url) {
        if (sslFactory == null) {
            KeyStore keyStore;
            try {
                keyStore = KeyStore.getInstance("JKS");
                keyStore.load(HttpClientSingleton.class.getResourceAsStream("/com/xxmicloxx/modmanager/certs/keystore.jks"), null);
                TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                tmf.init(keyStore);
                SSLContext ctx = SSLContext.getInstance("TLS");
                ctx.init(null, tmf.getTrustManagers(), null);
                sslFactory = ctx.getSocketFactory();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            } catch (CertificateException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }
        }
        HttpsURLConnection conn = null;
        try {
            conn = (HttpsURLConnection) url.openConnection();
            conn.setSSLSocketFactory(sslFactory);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return conn;
    }
}

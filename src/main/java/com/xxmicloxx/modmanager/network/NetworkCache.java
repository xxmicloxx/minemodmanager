package com.xxmicloxx.modmanager.network;

import com.google.common.util.concurrent.ListenableFuture;
import com.xxmicloxx.modmanager.ConfigManager;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import javax.net.ssl.HttpsURLConnection;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.concurrent.Callable;

/**
 * @author miclo
 */
public class NetworkCache {

    public static ListenableFuture<File> get(final String urlStr) {
        return HttpClientSingleton.executorService.submit(new Callable<File>() {
            @Override
            public File call() throws Exception {
                URL url = new URL(urlStr);
                File file = getCacheFile(url);
                HttpsURLConnection conn = HttpClientSingleton.getConnection(url);
                if (file.exists()) {
                    conn.setRequestProperty("If-None-Match", FileUtils.readFileToString(new File(file.getParentFile(), file.getName() + ".etag")));
                }
                conn.setDoInput(true);

                if (isCached(file, url, conn)) {
                    System.out.println(urlStr + " cached and etag matched...");
                    return file;
                }
                file.getParentFile().mkdirs();
                file.createNewFile();
                File etagFile = new File(file.getParentFile(), file.getName() + ".etag");
                IOUtils.copy(conn.getInputStream(), new FileOutputStream(file));
                etagFile.createNewFile();
                String etag = conn.getHeaderField("Etag");
                FileUtils.writeStringToFile(etagFile, etag);
                System.out.println(urlStr + " downloaded");
                return file;
            }
        });
    }

    private static File getCacheFile(URL url) throws MalformedURLException {
        String path = url.getPath();
        //trim
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        if (path.endsWith("/")) {
            path = path.substring(0, path.length() - 1);
        }
        File beginFile = new File(new File(new File(ConfigManager.getInstance().getMinecraftDir()), "cache"), url.getHost());
        String[] paths = path.split("/");
        for (String path1 : paths) {
            beginFile = new File(beginFile, path1);
        }
        return beginFile;
    }

    private static boolean isCached(File file, URL url, HttpsURLConnection resp) throws URISyntaxException, IOException {
        return resp.getResponseCode() == 304;
    }
}

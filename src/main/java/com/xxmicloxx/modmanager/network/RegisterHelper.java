package com.xxmicloxx.modmanager.network;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.Gson;
import com.xxmicloxx.modmanager.gson.GsonSingleton;

import javax.net.ssl.HttpsURLConnection;
import java.net.URL;
import java.util.concurrent.Callable;

/**
 * @author miclo
 */
public class RegisterHelper {

    public static ListenableFuture<RandomQuestion> getRandomQuestion() {
        final String requestUrl = HttpClientSingleton.HELPER_URL + "?verification";
        return HttpClientSingleton.executorService.submit(new Callable<RandomQuestion>() {
            @Override
            public RandomQuestion call() throws Exception {
                URL url = new URL(requestUrl);
                HttpsURLConnection connection = HttpClientSingleton.getConnection(url);
                connection.setDoInput(true);
                String text = HttpClientSingleton.readAllLines(connection.getInputStream());
                Gson gson = GsonSingleton.getInstance();
                return gson.fromJson(text, RandomQuestion.class);
            }
        });
    }

    public class RandomQuestion {

        private String question;
        private int id;

        public RandomQuestion() {}

        public RandomQuestion(String question, int id) {
            this.question = question;
            this.id = id;
        }

        public String getQuestion() {
            return question;
        }

        public int getId() {
            return id;
        }
    }
}

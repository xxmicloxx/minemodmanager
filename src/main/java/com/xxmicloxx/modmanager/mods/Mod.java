package com.xxmicloxx.modmanager.mods;

import com.dev4win.ModType;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: ml
 * Date: 27.10.13
 * Time: 22:40
 */
public class Mod {
    protected String name;
    protected String file;
    protected boolean isLocal = false;
    protected ModType modType;
    protected String artifactId;
    protected String version;

    public String getName() {
        return name;
    }

    public File getFile() {
        return new File(file);
    }

    public boolean isLocal() {
        return isLocal;
    }

    public ModType getModType() {
        return modType;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public String getVersion() {
        return version;
    }

    public Mod() {}

    public Mod(File file, ModType modType) {
        this.file = file.getAbsolutePath();
        name = file.getName();
        isLocal = true;
        this.modType = modType;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Mod)) return false;

        Mod mod = (Mod) o;

        if (isLocal != mod.isLocal) return false;
        if (artifactId != null ? !artifactId.equals(mod.artifactId) : mod.artifactId != null) return false;
        if (name != null ? !name.equals(mod.name) : mod.name != null) return false;
        if (modType != mod.modType) return false;
        if (version != null ? !version.equals(mod.version) : mod.version != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (isLocal ? 1 : 0);
        result = 31 * result + (modType != null ? modType.hashCode() : 0);
        result = 31 * result + (artifactId != null ? artifactId.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        return result;
    }
}

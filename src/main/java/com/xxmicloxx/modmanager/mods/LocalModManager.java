package com.xxmicloxx.modmanager.mods;

import com.dev4win.ModType;
import com.xxmicloxx.modmanager.ConfigManager;
import com.xxmicloxx.modmanager.events.EventManager;
import com.xxmicloxx.modmanager.events.mods.LocalModsChangedEvent;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ml
 * Date: 27.10.13
 * Time: 21:20
 */
public class LocalModManager {
    private static LocalModManager instance;
    public static LocalModManager getInstance() {
        if (instance == null) {
            instance = new LocalModManager();
            instance.init();
        }
        return instance;
    }

    private ArrayList<Mod> mods = new ArrayList<Mod>();

    private void init() {
        refreshMods();
    }

    public void refreshMods() {
        mods.clear();
        if (!ConfigManager.getInstance().getModsDir().exists()) {
            ConfigManager.getInstance().getModsDir().mkdirs();
        }
        for (File f : ConfigManager.getInstance().getJarModsDir().listFiles()) {
            if (f.isDirectory()) {
                continue;
            }
            String extension = FilenameUtils.getExtension(f.getAbsolutePath());
            if (!extension.equals("jar") && !extension.equals("zip")) {
                continue;
            }

            mods.add(new Mod(f, ModType.JAR));
        }
        for (File f : ConfigManager.getInstance().getModsModsDir().listFiles()) {
            if (f.isDirectory()) {
                continue;
            }
            String extension = FilenameUtils.getExtension(f.getAbsolutePath());
            if (!extension.equals("jar") && !extension.equals("zip")) {
                continue;
            }

            mods.add(new Mod(f, ModType.MODS));
        }
        EventManager.post(new LocalModsChangedEvent());
    }

    public ArrayList<Mod> getMods() {
        return mods;
    }
}

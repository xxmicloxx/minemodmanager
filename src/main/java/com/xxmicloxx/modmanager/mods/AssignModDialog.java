package com.xxmicloxx.modmanager.mods;

import com.alee.laf.list.WebList;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.scroll.WebScrollPane;
import com.xxmicloxx.modmanager.MainWindow;
import com.xxmicloxx.modmanager.instances.Instance;
import com.xxmicloxx.modmanager.instances.InstanceManager;

import javax.swing.*;
import java.awt.event.*;

public class AssignModDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private WebScrollPane webScrollPane1;
    private WebList webList1;

    private Mod mod;

    private void createUIComponents() {
        webScrollPane1 = new WebScrollPane(webList1 = new WebList());
        webScrollPane1.setDrawBorder(true);
        webScrollPane1.setDrawFocus(true);
        webList1.setEditable(false);
        DefaultListModel<Instance> dlm = new DefaultListModel<Instance>();
        for (Instance i : InstanceManager.getInstance().getInstances()) {
            dlm.addElement(i);
        }
        //noinspection unchecked
        webList1.setModel(dlm);
    }

    public AssignModDialog(Mod mod) {
        this.mod = mod;
        setTitle("Assign mod");
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        webList1.setEditable(false);
        webList1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        webList1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                if (mouseEvent.getButton() != 1) {
                    return;
                }
                if (mouseEvent.getClickCount() % 2 == 0) {
                    buttonOK.doClick();
                }
            }
        });

        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        pack();
        setLocationRelativeTo(MainWindow.getInstance().getWindow());
        setVisible(true);
    }

    private void mergeMod(final Instance inst, final Mod mod) {
        if (inst.hasMod(mod)) {
            WebOptionPane.showMessageDialog(this, "This mod has already been added to the selected instance!", "Error", JOptionPane.WARNING_MESSAGE);
            return;
        }
        inst.getMods().add(mod);
        inst.setDirty();
        InstanceManager.getInstance().save(inst);
        dispose();
        /*new Thread(new Runnable() {
            @Override
            public void run() {
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        progress.setIndeterminate(false);
                        progress.setStringPainted(true);
                        progress.setString("Copying files...");
                        progress.setIndeterminate(true);
                    }
                });
                File targFile = new File(ConfigManager.getInstance().getInstanceDir(), inst.getName() + "/" + inst.getName() + ".zip");
                targFile.delete();
                File srcFile = new File(ConfigManager.getInstance().getVersionDir(inst.getVersion()), inst.getVersion().getId() + ".zip");
                FileUtils.copyFile(srcFile, targFile);
                targFile.renameTo(new File(ConfigManager.getInstance().getInstanceDir(), inst.getName() + "/" + inst.getName() + ".zip"));
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        progress.setIndeterminate(false);
                        progress.setString("Unpacking...");
                        progress.setValue(0);
                    }
                });
                File unpackFolder = new File(targFile.getParentFile(), "upck");
                unpackFolder.mkdir();
                try {
                    ZipFile zipped = new ZipFile(targFile);
                    if (zipped.isEncrypted()) {
                        zipped.setPassword(LocalVersionManager.ZIP_CYPHER);
                    }
                    zipped.extractAll(unpackFolder.getAbsolutePath());
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            progress.setValue(33);
                        }
                    });
                    File mcjf = new File(unpackFolder, inst.getVersion().getId() + ".jar");
                    ZipFile mcJar = new ZipFile(mcjf);
                    File unpackFolder2 = new File(unpackFolder, "mcj");
                    unpackFolder2.mkdir();
                    mcJar.extractAll(unpackFolder2.getAbsolutePath());
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            progress.setValue(80);
                        }
                    });
                    for (Mod m : inst.getMods()) {
                        if (m.getType() != Mod.Type.JAR) {
                            continue;
                        }
                        ZipFile mcMod = new ZipFile(m.getFile());
                        File modFolder = new File(unpackFolder, "mcm");
                        modFolder.mkdir();
                        mcMod.extractAll(modFolder.getAbsolutePath());
                        FileUtils.copyDirectory(modFolder, unpackFolder2);
                        org.apache.commons.io.FileUtils.deleteDirectory(modFolder);
                    }
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            progress.setString("Packing...");
                            progress.setIndeterminate(false);
                            progress.setValue(0);
                        }
                    });
                    mcjf.delete();
                    ZipFile targetZip = new ZipFile(mcjf);
                    ZipParameters parameters = new ZipParameters();
                    zipMe(unpackFolder2, targetZip, parameters);
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            progress.setValue(90);
                        }
                    });
                    org.apache.commons.io.FileUtils.deleteDirectory(unpackFolder2);
                    targFile.delete();
                    File targetModDir = new File(unpackFolder, "mods");
                    for (Mod m : inst.getMods()) {
                        if (m.getType() != Mod.Type.MODS) {
                            continue;
                        }
                        FileUtils.copyFile(m.getFile(), new File(targetModDir, m.getFile().getName()));
                    }
                    zipped = new ZipFile(targFile);
                    parameters = new ZipParameters();
                    parameters.setPassword(LocalVersionManager.ZIP_CYPHER);
                    parameters.setEncryptFiles(true);
                    parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
                    parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
                    zipMe(unpackFolder, zipped, parameters);
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            progress.setValue(100);
                            progress.setString("Cleaning up...");
                            progress.setIndeterminate(true);
                        }
                    });
                    org.apache.commons.io.FileUtils.deleteDirectory(unpackFolder);
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            progress.setString("Finished!");
                            progress.setValue(100);
                            progress.setIndeterminate(false);
                        }
                    });
                    Thread.sleep(1000);
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            AssignModDialog.this.setEnabled(true);
                            AssignModDialog.this.dispose();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, "JAR creator thread").start();*/
    }

    /*private void zipMe(File folder, ZipFile targ, ZipParameters param) throws ZipException {
        boolean first = true;
        for (File f : folder.listFiles()) {
            if (f.isDirectory()) {
                if (!first) {
                    targ.addFolder(f, param);
                } else {
                    targ.createZipFileFromFolder(f, param, false, 0);
                }
            } else {
                if (!first) {
                    targ.addFile(f, param);
                } else {
                    targ.createZipFile(f, param);
                }
            }
            first = false;
        }
    }*/

    private void onOK() {
        if (webList1.getSelectedIndex() == -1) {
            // no instance selected
            WebOptionPane.showMessageDialog(AssignModDialog.this, "You have to select an instance!", "Error", JOptionPane.WARNING_MESSAGE);
            return;
        }
        Instance i = (Instance) ((DefaultListModel) webList1.getModel()).elementAt(webList1.getSelectedIndex());
        mergeMod(i, mod);
    }

    private void onCancel() {
        dispose();
    }
}

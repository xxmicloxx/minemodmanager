package com.xxmicloxx.modmanager.instances;

import com.alee.laf.optionpane.WebOptionPane;
import com.xxmicloxx.modmanager.ConfigManager;
import com.xxmicloxx.modmanager.MainWindow;
import com.xxmicloxx.modmanager.yggdrasil.AuthenticationException;
import com.xxmicloxx.modmanager.yggdrasil.Yggdrasil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.concurrent.ExecutionException;

public class LoginDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtUsername;
    private JPasswordField txtPassword;
    private JLabel lblUsername;
    private JLabel lblPassword;
    private JCheckBox chkSaveUser;
    private JCheckBox chkSavePassword;
    private JPanel elementsPane;
    private JProgressBar mainProgressBar;

    private boolean wasOkay = false;
    private String accessToken;

    public LoginDialog() {
        setTitle("Login");
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        pack();
        setLocationRelativeTo(MainWindow.getInstance().getWindow());

        txtUsername.setPreferredSize(txtUsername.getSize());
        txtPassword.setPreferredSize(txtPassword.getSize());

        if (ConfigManager.getInstance().getUsername() != null) {
            txtUsername.setText(ConfigManager.getInstance().getUsername());
            chkSaveUser.setSelected(true);
        }
        if (ConfigManager.getInstance().getEncPassword() != null) {
            txtPassword.setText(ConfigManager.getInstance().getEncPassword());
            chkSavePassword.setSelected(true);
        }

        mainProgressBar.setPreferredSize(new Dimension(50, (int) mainProgressBar.getPreferredSize().getHeight()));

        setVisible(true);
    }

    public String getUsername() {
        return txtUsername.getText();
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getPassword() {
        return String.valueOf(txtPassword.getPassword());
    }

    public boolean shouldSaveUsername() {
        return chkSaveUser.isSelected();
    }

    public boolean shouldSavePassword() {
        return chkSavePassword.isSelected();
    }

    public boolean wasOkay() {
        return wasOkay;
    }

    private void onOK() {
        if (getUsername().isEmpty()) {
            WebOptionPane.showMessageDialog(this, "You have to enter a username!", "Error", JOptionPane.WARNING_MESSAGE);
            return;
        }
        if (getPassword().isEmpty()) {
            WebOptionPane.showMessageDialog(this, "You have to enter a password!", "Error", JOptionPane.WARNING_MESSAGE);
            //Yggdrasil incoming -,-
            return;
        }
        mainProgressBar.setVisible(true);
        pack();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (ConfigManager.getInstance().getEncPassword() != null && (ConfigManager.getInstance().getEncPassword().equals(getPassword()) && ConfigManager.getInstance().getUsername().equals(getUsername()))) {
                        // refresh
                        accessToken = Yggdrasil.getInstance().refresh().get();
                        if (shouldSavePassword()) {
                            ConfigManager.getInstance().setEncPassword(accessToken);
                        }
                        if (!shouldSavePassword()) {
                            ConfigManager.getInstance().setEncPassword(null);
                        }
                        if (!shouldSaveUsername() && !shouldSavePassword()) {
                            ConfigManager.getInstance().setUsername(null);
                        }
                        ConfigManager.getInstance().validateConfig();
                        wasOkay = true;
                        EventQueue.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                dispose();
                            }
                        });
                    } else if (ConfigManager.getInstance().getEncPassword() != null && !(ConfigManager.getInstance().getEncPassword().equals(getPassword()) && ConfigManager.getInstance().getUsername().equals(getUsername()))) {
                        Yggdrasil.getInstance().invalidate().get();
                        accessToken = Yggdrasil.getInstance().authenticate(getUsername(), getPassword()).get();
                        if (shouldSaveUsername() || shouldSavePassword()) {
                            ConfigManager.getInstance().setUsername(getUsername());
                        }
                        if (shouldSavePassword()) {
                            ConfigManager.getInstance().setEncPassword(accessToken);
                        }
                        if (!shouldSavePassword()) {
                            ConfigManager.getInstance().setEncPassword(null);
                        }
                        if (!shouldSaveUsername() && !shouldSavePassword()) {
                            ConfigManager.getInstance().setUsername(null);
                        }
                        ConfigManager.getInstance().validateConfig();
                        wasOkay = true;
                        EventQueue.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                dispose();
                            }
                        });
                    } else {
                        accessToken = Yggdrasil.getInstance().authenticate(getUsername(), getPassword()).get();
                        if (shouldSaveUsername() || shouldSavePassword()) {
                            ConfigManager.getInstance().setUsername(getUsername());
                        }
                        if (shouldSavePassword()) {
                            ConfigManager.getInstance().setEncPassword(accessToken);
                        }
                        if (!shouldSavePassword()) {
                            ConfigManager.getInstance().setEncPassword(null);
                        }
                        if (!shouldSaveUsername() && !shouldSavePassword()) {
                            ConfigManager.getInstance().setUsername(null);
                        }
                        ConfigManager.getInstance().validateConfig();
                        wasOkay = true;
                        EventQueue.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                dispose();
                            }
                        });
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (final ExecutionException e) {
                    if (e.getCause() instanceof AuthenticationException) {
                        EventQueue.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                WebOptionPane.showMessageDialog(LoginDialog.this, ((AuthenticationException) e.getCause()).getErrorMessage(), "Error", JOptionPane.WARNING_MESSAGE);
                            }
                        });
                    } else {
                        e.printStackTrace();
                    }
                    mainProgressBar.setVisible(false);
                    pack();
                }
            }
        }, "Login thread").start();
    }

    private void onCancel() {
        dispose();
    }
}

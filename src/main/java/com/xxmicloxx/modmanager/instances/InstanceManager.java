package com.xxmicloxx.modmanager.instances;

import com.alee.laf.optionpane.WebOptionPane;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.xxmicloxx.modmanager.ConfigManager;
import com.xxmicloxx.modmanager.MainWindow;
import com.xxmicloxx.modmanager.events.EventManager;
import com.xxmicloxx.modmanager.events.instances.InstancesChangedEvent;
import com.xxmicloxx.modmanager.gson.GsonSingleton;
import org.apache.commons.io.FileUtils;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ml
 * Date: 13.10.13
 * Time: 19:26
 */
public class InstanceManager {
    private static InstanceManager instance;
    public static InstanceManager getInstance() {
        if (instance == null) {
            instance = new InstanceManager();
            instance.init();
        }
        return instance;
    }

    private ArrayList<Instance> instances = new ArrayList<Instance>();

    private void init() {
        // read all instances
        Gson gson = GsonSingleton.getInstance();
        for (File instance : ConfigManager.getInstance().getInstanceDir().listFiles()) {
            if (!instance.isDirectory()) {
                continue;
            }
            File contentFile = new File(instance, instance.getName() + ".json");
            if (!contentFile.exists() || contentFile.isDirectory()) {
                continue;
            }
            try {
                Instance i = gson.fromJson(FileUtils.readFileToString(contentFile), Instance.class);
                //TODO some status checking
                instances.add(i);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JsonSyntaxException e) {
                WebOptionPane.showMessageDialog(null, "Error while parsing instance \"" + instance.getName() + "\": " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        EventManager.post(new InstancesChangedEvent());
    }

    public ArrayList<Instance> getInstances() {
        return instances;
    }

    public boolean doesInstanceExist(String name) {
        for (Instance i : instances) {
            if (i.getName() != null && i.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public void save(Instance i) {
        File instanceFile = new File(ConfigManager.getInstance().getInstanceDir(), i.getName() + "/" + i.getName() + ".json");
        instanceFile.getParentFile().mkdirs();
        Gson gson = GsonSingleton.getInstance();
        try {
            FileUtils.writeStringToFile(instanceFile, gson.toJson(i));
        } catch (IOException e) {
            WebOptionPane.showMessageDialog(null, "Error while trying to save instance \"" + i.getName() + "\": " + e.getMessage());
        }
    }

    public void delete(Instance i) {
        File instanceDir = new File(ConfigManager.getInstance().getInstanceDir(), i.getName());
        try {
            FileUtils.deleteDirectory(instanceDir);
        } catch (IOException e) {
            WebOptionPane.showMessageDialog(null, "Error while trying to delete instance \"" + i.getName() + "\": " + e.getMessage());
        }
        instances.remove(i);
        EventManager.post(new InstancesChangedEvent());
    }
}

package com.xxmicloxx.modmanager.instances;

import com.alee.extended.window.WebProgressDialog;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.utils.FileUtils;
import com.xxmicloxx.modmanager.ConfigManager;
import com.xxmicloxx.modmanager.MainWindow;
import com.xxmicloxx.modmanager.OperatingSystem;
import com.xxmicloxx.modmanager.dialogs.LibraryDownloadDialog;
import com.xxmicloxx.modmanager.gson.GsonSingleton;
import com.xxmicloxx.modmanager.network.LibraryDownloader;
import com.xxmicloxx.modmanager.panels.tabs.LogTab;
import com.xxmicloxx.modmanager.process.JavaProcess;
import com.xxmicloxx.modmanager.process.JavaProcessLauncher;
import com.xxmicloxx.modmanager.process.JavaProcessRunnable;
import com.xxmicloxx.modmanager.versions.*;
import com.xxmicloxx.modmanager.versions.assets.AssetIndex;
import com.xxmicloxx.modmanager.yggdrasil.Yggdrasil;
import de.schlichtherle.truezip.file.TFile;
import de.schlichtherle.truezip.file.TVFS;
import de.schlichtherle.truezip.fs.FsSyncException;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.text.StrSubstitutor;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: ml
 * Date: 28.10.13
 * Time: 20:38
 */
public class InstanceStarter implements JavaProcessRunnable {
    private Instance instance;
    private Runnable onCompleteListener;
    private Runnable onCancelListener;

    public InstanceStarter(Instance i) {
        this.instance = i;
    }

    public void start() {
        if (instance.isDirty()) {
            InstancePacker ip = new InstancePacker(instance);
            ip.setDoneListener(new Runnable() {
                @Override
                public void run() {
                    login();
                }
            });
            ip.start();
        } else {
            login();
        }
    }

    public Runnable getOnCancelListener() {
        return onCancelListener;
    }

    public void setOnCancelListener(Runnable onCancelListener) {
        this.onCancelListener = onCancelListener;
    }

    public Runnable getOnCompleteListener() {
        return onCompleteListener;
    }

    public void setOnCompleteListener(Runnable onCompleteListener) {
        this.onCompleteListener = onCompleteListener;
    }

    private void login() {
        // login
        final LoginDialog ld = new LoginDialog();
        if (!ld.wasOkay()) {
            if (onCancelListener != null) {
                onCancelListener.run();
            }
            return;
        }
        final String accessToken = ld.getAccessToken();
        final WebProgressDialog dialog = new WebProgressDialog(MainWindow.getInstance().getWindow(), "Starting instance...");
        // do the resource check
        dialog.setModal(true);
        dialog.setLocationRelativeTo(MainWindow.getInstance().getWindow());
        dialog.setShowCloseButton(false);
        dialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        VersionDownloader.downloadResources(dialog, new Runnable() {
            @Override
            public void run() {
                dialog.setProgressText("Processing start...");
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (instance.getAdditionalLibraries() != null && instance.getAdditionalLibraries().size() != 0) {
                            TFile additionalLibrariesFolder = new TFile(instance.getInstanceDir(), "additionalLibraries");
                            additionalLibrariesFolder.mkdirs();
                            try {
                                Set<Downloadable> downloadables = instance.getRequiredDownloadables(OperatingSystem.getCurrentPlatform(), additionalLibrariesFolder);
                                new LibraryDownloadDialog(downloadables, dialog).addLibraryDownloadListener(new LibraryDownloadDialog.LibraryDownloadListener() {
                                    @Override
                                    public void onDownloadFinished() {
                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                startMinecraft(dialog, accessToken);
                                            }
                                        }, "Minecraft starter #2").start();
                                    }
                                });
                                new LibraryDownloader(downloadables);
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                        } else {
                            startMinecraft(dialog, accessToken);
                        }
                    }
                }, "Minecraft starter").start();
            }
        }, instance.getVersion());
    }

    private void startMinecraft(final WebProgressDialog dialog, String accessToken) {
        File assetsDir;
        try {
            assetsDir = reconstructAssets(dialog);
        } catch (IOException e) {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    dialog.dispose();
                    WebOptionPane.showMessageDialog(MainWindow.getInstance().getWindow(), "Error while unpacking resources!", "Error", JOptionPane.ERROR_MESSAGE);
                }
            });
            return;
        }
        TFile gameDirectory = new TFile(instance.getRunningDir());
        TFile nativesDir = new TFile(gameDirectory, "natives");
        FileUtils.clearDirectory(nativesDir);
        unpackNatives(instance.getVersion(), nativesDir, dialog);
        JavaProcessLauncher processLauncher = new JavaProcessLauncher(null, instance);
        processLauncher.getDirectory(gameDirectory);
        if (OperatingSystem.getCurrentPlatform().equals(OperatingSystem.WINDOWS)) {
            //fuck windows.
            System.out.println("Windows sucks. Get an operating system. Now.");
            processLauncher.addCommands("-XX:HeapDumpPath=Dev4WinTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump");
        }
        processLauncher.addSplitCommands("-Xmx" + instance.getRam() + "M");
        processLauncher.addCommands("-Djava.library.path=" + nativesDir.getAbsolutePath());
        if (instance.getAdditionalVMParameters() != null) {
            processLauncher.addSplitCommands(instance.getAdditionalVMParameters());
        }
        processLauncher.addCommands("-cp", constructClassPath(instance.getVersion()));
        processLauncher.addCommands(instance.getMainClass() != null ? instance.getMainClass() : instance.getVersion().getMainClass());
        String[] args = getMinecraftArguments(accessToken, assetsDir);
        processLauncher.addCommands(args);
        if (instance.getAdditionalParams() != null) {
            processLauncher.addSplitCommands(instance.getAdditionalParams());
        }
        try {
            JavaProcess process = processLauncher.start();
            process.safeSetExitRunnable(this);
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    instance.setRunningInstances(instance.getRunningInstances()+1);
                    dialog.dispose();
                    MainWindow.getInstance().getWindow().setState(Frame.ICONIFIED);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File reconstructAssets(WebProgressDialog dialog) throws IOException {
        File assetsDir = ConfigManager.getInstance().getResourcesDir();
        File indexDir = new File(assetsDir, "indexes");
        File objectDir = new File(assetsDir, "objects");
        String assetVersion = instance.getVersion().getAssets() == null ? "legacy" : instance.getVersion().getAssets();
        File indexFile = new File(indexDir, assetVersion + ".json");
        File virtualRoot = new File(new File(assetsDir, "virtual"), assetVersion);
        if (!indexFile.isFile()) {
            System.out.println("WARNING: assets file not found: " + indexFile);
            return virtualRoot;
        }
        AssetIndex index = GsonSingleton.getInstance().fromJson(org.apache.commons.io.FileUtils.readFileToString(indexFile, Charset.forName("UTF-8")), AssetIndex.class);
        if (index.isVirtual()) {
            System.out.println("Reconstructing virtual assets folder");
            for (Map.Entry<String, AssetIndex.AssetObject> entry : index.getFileMap().entrySet()) {
                File target = new File(virtualRoot, entry.getKey());
                File original = new File(new File(objectDir, entry.getValue().getHash().substring(0, 2)), entry.getValue().getHash());
                if (!target.isFile()) {
                    FileUtils.copyFile(original, target);
                }
            }
        }
        return virtualRoot;
    }

    private String[] getMinecraftArguments(String accessToken, File assetsDir) {
        if (instance.getVersion().getMinecraftArguments() == null) {
            throw new RuntimeException("Version does not have arguments -,-");
        }
        Map<String, String> map = new HashMap<String, String>();
        StrSubstitutor substitutor = new StrSubstitutor(map);
        String[] split = instance.getVersion().getMinecraftArguments().split(" ");
        map.put("auth_access_token", accessToken);
        map.put("user_properties", "{}");
        map.put("auth_session", String.format("token:%s:%s", accessToken, Yggdrasil.getInstance().getLastProfile().getId()));
        map.put("auth_player_name", Yggdrasil.getInstance().getLastProfile().getName());
        map.put("auth_uuid", Yggdrasil.getInstance().getLastProfile().getId());
        map.put("profile_name", instance.getName());
        map.put("version_name", instance.getVersion().getId());
        map.put("game_directory", instance.getRunningDir().getAbsolutePath());
        map.put("game_assets", assetsDir.getAbsolutePath());
        map.put("assets_root", ConfigManager.getInstance().getResourcesDir().getAbsolutePath());
        map.put("assets_index_name", instance.getVersion().getAssets() == null ? "legacy" : instance.getVersion().getAssets());
        for (int i = 0; i < split.length; i++) {
            split[i] = substitutor.replace(split[i]);
        }
        return split;
    }

    private String constructClassPath(Version version) {
        StringBuilder result = new StringBuilder();
        Collection<File> classPath = version.getClassPath(OperatingSystem.getCurrentPlatform(), instance.getRunningDir());
        String separator = System.getProperty("path.separator");
        for (File file : classPath) {
            if (!file.isFile()) {
                throw new RuntimeException("Classpath entry is no file: " + file);
            }
            if (result.length() > 0) {
                result.append(separator);
            }
            result.append(file.getAbsolutePath());
        }
        classPath = instance.getClassPath(OperatingSystem.getCurrentPlatform(), new File(instance.getInstanceDir(), "additionalLibraries"));
        for (File file : classPath) {
            if (!file.isFile()) {
                throw new RuntimeException("Classpath entry is no file: " + file);
            }
            if (result.length() > 0) {
                result.append(separator);
            }
            result.append(file.getAbsolutePath());
        }
        File customLibrariesFolder = new File(instance.getInstanceDir(), "customLibraries");
        customLibrariesFolder.mkdirs();
        for (File f : customLibrariesFolder.listFiles()) {
            if (f.isDirectory() || !f.isFile()) {
                continue;
            }
            if (!FilenameUtils.isExtension(f.getName(), new String[]{"jar", "zip"})) {
                continue;
            }
            if (result.length() > 0) {
                result.append(separator);
            }
            result.append(f.getAbsolutePath());
        }
        if (result.length() > 0) {
            result.append(separator);
        }
        result.append(new File(instance.getRunningDir(), instance.getVersion().getId() + ".jar").getAbsolutePath());
        return result.toString();
    }

    private void unpackNatives(Version version, File nativesDir, WebProgressDialog dialog) {
        dialog.setIndeterminate(true);
        OperatingSystem os = OperatingSystem.getCurrentPlatform();
        Collection<Library> libraries = version.getRelevantLibraries();
        for (Library library : libraries) {
            Map<OperatingSystem, String> nativesPerOs = library.getNatives();
            if (nativesPerOs != null && nativesPerOs.get(os) != null) {
                TFile zipFile = new TFile(instance.getRunningDir(), "libraries/" + library.getArtifactPath(nativesPerOs.get(os)));
                ExtractRules extractRules = library.getExtract();
                try {
                    for (TFile entry : zipFile.listFiles()) {
                        if (extractRules != null && !extractRules.shouldExtract(entry.getName())) {
                            continue;
                        }
                        File targetFile = new File(nativesDir, entry.getName());
                        if (targetFile.getParentFile() != null) {
                            targetFile.getParentFile().mkdirs();
                        }
                        if (entry.isDirectory()) {
                            continue;
                        }
                        try {
                            entry.cp(targetFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } finally {
                    try {
                        TVFS.umount(zipFile);
                    } catch (FsSyncException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onJavaProcessEnded(final JavaProcess p0, Instance instance) {
        System.out.println("Instance '" + instance.getName() + "' ended with exit code " + p0.getExitCode());
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainWindow.getInstance().getWindow().setState(Frame.NORMAL);
                if (p0.getExitCode() != 0) {
                    MainWindow.getInstance().showTab(p0.getInstanceWatcher());
                }
                if (onCompleteListener != null) {
                    onCompleteListener.run();
                }
            }
        });
    }
}

package com.xxmicloxx.modmanager.instances;

import com.alee.extended.panel.GroupPanel;
import com.alee.laf.list.WebList;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.scroll.WebScrollPane;
import com.xxmicloxx.modmanager.MainWindow;
import com.xxmicloxx.modmanager.events.EventManager;
import com.xxmicloxx.modmanager.events.instances.InstancesChangedEvent;
import com.xxmicloxx.modmanager.versions.LocalVersionManager;
import com.xxmicloxx.modmanager.versions.Version;

import javax.swing.*;
import java.awt.event.*;

public class InstanceCreationDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtName;
    private WebList lstVersions;
    private JLabel lblName;
    private JLabel lblVersion;
    private GroupPanel groupPanel1;
    private WebScrollPane webScrollPane1;

    public void createUIComponents() {
        groupPanel1 = new GroupPanel(webScrollPane1 = new WebScrollPane(lstVersions = new WebList()));
        webScrollPane1.setDrawBorder(false);
        groupPanel1.setUndecorated(false);
        groupPanel1.setDrawFocus(true);
    }

    public InstanceCreationDialog() {
        super(MainWindow.getInstance().getWindow());
        //TODO i18n

        setTitle("Create new instance");

        DefaultListModel<Version> dlm = new DefaultListModel<Version>();
        for (Version v : LocalVersionManager.getInstance().getVersions()) {
            dlm.addElement(v);
        }
        //noinspection unchecked
        lstVersions.setModel(dlm);
        lstVersions.setEditable(false);
        lstVersions.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        lstVersions.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                if (mouseEvent.getButton() != 1) {
                    return;
                }
                if (mouseEvent.getClickCount() % 2 == 0) {
                    buttonOK.doClick();
                }
            }
        });

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        pack();
        setLocationRelativeTo(MainWindow.getInstance().getWindow());
        setVisible(true);
    }

    private void onOK() {
        if (txtName.getText().isEmpty()) {
            WebOptionPane.showMessageDialog(this, "Empty names are not allowed!", "Error", JOptionPane.WARNING_MESSAGE);
            return;
        }
        if (lstVersions.getSelectedIndex() == -1) {
            WebOptionPane.showMessageDialog(this, "Please select a version!", "Error", JOptionPane.WARNING_MESSAGE);
            return;
        }
        if (InstanceManager.getInstance().doesInstanceExist(txtName.getText())) {
            WebOptionPane.showMessageDialog(this, "The entered instance name already exists!", "Error", JOptionPane.WARNING_MESSAGE);
            return;
        }
        Instance i = new Instance(txtName.getText(), (Version) lstVersions.getSelectedValue());
        InstanceManager.getInstance().getInstances().add(i);
        InstanceManager.getInstance().save(i);
        EventManager.post(new InstancesChangedEvent());
        dispose();
    }

    private void onCancel() {
        dispose();
    }
}

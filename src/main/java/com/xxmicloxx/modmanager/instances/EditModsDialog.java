package com.xxmicloxx.modmanager.instances;

import com.alee.extended.panel.GroupPanel;
import com.alee.laf.StyleConstants;
import com.alee.laf.button.WebButton;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.table.WebTable;
import com.alee.laf.toolbar.ToolbarStyle;
import com.alee.laf.toolbar.WebToolBar;
import com.xxmicloxx.modmanager.icons.IconHelper;
import com.xxmicloxx.modmanager.mods.Mod;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;

public class EditModsDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private GroupPanel mainGroupPane;
    private WebToolBar mainToolBar;
    private WebScrollPane modsScrollPane;
    private WebTable mainTable;

    private Instance instance;
    private WebButton deleteWebButton;

    private void createUIComponents() {
        mainGroupPane = new GroupPanel(mainToolBar = new WebToolBar(), modsScrollPane = new WebScrollPane(mainTable = new WebTable()));
        modsScrollPane.setDrawBorder(false);
        mainGroupPane.setUndecorated(false);
        mainGroupPane.setDrawFocus(true);
        mainToolBar.setToolbarStyle(ToolbarStyle.attached);
    }

    public EditModsDialog(Instance i) {
        this.instance = i;
        setTitle("Manage Mods");
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        refreshTable();
        mainTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                if (mainTable.getSelectedColumn() != -1) {
                    setButtonsEnabled(true);
                } else {
                    setButtonsEnabled(false);
                }
            }
        });
        createToolbar();
        pack();
    }

    private void setButtonsEnabled(boolean enabled) {
        deleteWebButton.setEnabled(enabled);
    }

    private void createToolbar() {
        deleteWebButton = WebButton.createIconWebButton(IconHelper.loadIcon("/com/xxmicloxx/modmanager/icons/trash.png"), StyleConstants.smallRound, true);
        deleteWebButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Mod[] mods = fetchSelectedMods();
                for (Mod m : mods) {
                    instance.getMods().remove(m);
                }
                instance.setDirty();
                InstanceManager.getInstance().save(instance);
                int[] selectedRows = mainTable.getSelectedRows();
                DefaultTableModel dtm = (DefaultTableModel) mainTable.getModel();
                int deletedRows = 0;
                for (int row : selectedRows) {
                    dtm.removeRow(row - deletedRows);
                    deletedRows++;
                }
            }
        });
        deleteWebButton.setEnabled(false);
        mainToolBar.add(deleteWebButton);
        mainToolBar.setFloatable(false);
    }

    private Mod[] fetchSelectedMods() {
        int[] rows = mainTable.getSelectedRows();
        Mod[] mods = new Mod[rows.length];
        DefaultTableModel dtm = (DefaultTableModel) mainTable.getModel();
        for (int i = 0; i < rows.length; i++) {
            int row = rows[i];
            mods[i] = (Mod) dtm.getValueAt(row, 0);
        }
        return mods;
    }

    private void refreshTable() {
        DefaultTableModel dtm = new DefaultTableModel();
        dtm.addColumn("Name");
        dtm.addColumn("Type");
        for (Mod m : instance.getMods()) {
            dtm.addRow(new Object[]{m, m.getModType()});
        }
        mainTable.setEditable(false);
        mainTable.setModel(dtm);
    }

    private void onOK() {
        dispose();
    }

    private void onCancel() {
        dispose();
    }
}

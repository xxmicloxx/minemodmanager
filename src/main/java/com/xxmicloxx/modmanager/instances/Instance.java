package com.xxmicloxx.modmanager.instances;

import com.xxmicloxx.modmanager.ConfigManager;
import com.xxmicloxx.modmanager.OperatingSystem;
import com.xxmicloxx.modmanager.mods.Mod;
import com.xxmicloxx.modmanager.panels.tabs.InstanceWatcher;
import com.xxmicloxx.modmanager.versions.Downloadable;
import com.xxmicloxx.modmanager.versions.Library;
import com.xxmicloxx.modmanager.versions.LocalVersionManager;
import com.xxmicloxx.modmanager.versions.Version;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: ml
 * Date: 27.10.13
 * Time: 22:40
 */
public class Instance {
    private String name;
    private String version;
    private String description;
    private String additionalParams;
    private List<Library> additionalLibraries;
    private Set<Mod> mods = new HashSet<Mod>();
    private boolean dirty;
    private boolean shouldDeleteMetaInf;
    private String mainClass;
    private String additionalVMParameters;
    private transient int runningInstances = 0;

    public Instance() {}

    public Instance(String name, Version version) {
        this.version = version.getId();
        this.name = name;
        this.description = "";
        this.shouldDeleteMetaInf = false;
        this.additionalParams = null;
        this.additionalLibraries = new ArrayList<Library>();
        this.mainClass = null;
        setDirty();
    }

    public int getRunningInstances() {
        return runningInstances;
    }

    public void setRunningInstances(int runningInstances) {
        this.runningInstances = runningInstances;
    }

    public String getAdditionalVMParameters() {
        return additionalVMParameters;
    }

    public void setAdditionalVMParameters(String additionalVMParameters) {
        this.additionalVMParameters = additionalVMParameters;
    }

    public String getMainClass() {
        return mainClass;
    }

    public void setMainClass(String mainClass) {
        this.mainClass = mainClass;
    }

    public Collection<File> getClassPath(OperatingSystem os, File base) {
        if (additionalLibraries == null) {
            return new ArrayList<File>();
        }
        Collection<File> result = new ArrayList<File>();
        for (Library library : additionalLibraries) {
            if (library.getNatives() == null) {
                result.add(new File(base, library.getArtifactPath()));
            }
        }
        return result;
    }

    public Set<Downloadable> getRequiredDownloadables(OperatingSystem os, File targetDirectory) throws MalformedURLException {
        Set<Downloadable> neededFiles = new HashSet<Downloadable>();

        for (Library library : additionalLibraries) {
            String file = null;

            if (library.getNatives() != null) {
                String natives = library.getNatives().get(os);
                if (natives != null)
                    file = library.getArtifactPath(natives);
            }
            else {
                file = library.getArtifactPath();
            }

            if (file != null) {
                URL url = new URL(library.getDownloadUrl() + file);
                File local = new File(targetDirectory, file);
                File temp = new File(ConfigManager.getInstance().getLibraryCache(), file);

                if ((!local.isFile()) || (library.hasNoCustomUrl())) {
                    neededFiles.add(new Downloadable(url, local, temp));
                }
            }
        }

        return neededFiles;
    }

    public List<Library> getAdditionalLibraries() {
        return additionalLibraries;
    }

    public void setAdditionalLibraries(List<Library> additionalLibraries) {
        this.additionalLibraries = additionalLibraries;
    }

    public String getAdditionalParams() {
        return additionalParams;
    }

    public void setAdditionalParams(String additionalParams) {
        this.additionalParams = additionalParams;
    }

    public boolean shouldDeleteMetaInf() {
        return shouldDeleteMetaInf;
    }

    public void setShouldDeleteMetaInf(boolean shouldDeleteMetaInf) {
        this.shouldDeleteMetaInf = shouldDeleteMetaInf;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty() {
        setDirty(true);
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    public Version getVersion() {
        return LocalVersionManager.getInstance().getVersionById(version);
    }

    public int getRam() {
        return 2048;
    }

    public Set<Mod> getMods() {
        return mods;
    }

    public void setVersion(Version version) {
        this.version = version.getId();
    }

    public String getName() {
        return name;
    }

    public boolean hasMod(Mod m) {
        for (Mod mod : mods) {
            if (mod.equals(m)) {
                return true;
            }
        }
        return false;
    }

    public File getInstanceDir() {
        File instanceDir = new File(ConfigManager.getInstance().getInstanceDir(), name);
        instanceDir.mkdirs();
        return instanceDir;
    }

    public File getRunningDir() {
        File runningDir = new File(getInstanceDir(), "runtime");
        runningDir.mkdirs();
        return runningDir;
    }

    @Override
    public String toString() {
        return name + " [" + version + "]";
    }
}

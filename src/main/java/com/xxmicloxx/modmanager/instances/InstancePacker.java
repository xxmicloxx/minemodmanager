package com.xxmicloxx.modmanager.instances;

import com.alee.extended.window.WebProgressDialog;
import com.alee.utils.FileUtils;
import com.dev4win.ModType;
import com.xxmicloxx.modmanager.ConfigManager;
import com.xxmicloxx.modmanager.MainWindow;
import com.xxmicloxx.modmanager.mods.Mod;
import de.schlichtherle.truezip.file.TFile;
import de.schlichtherle.truezip.file.TVFS;
import de.schlichtherle.truezip.fs.FsSyncException;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Mic
 * Date: 12.11.13
 * Time: 19:00
 * To change this template use File | Settings | File Templates.
 */
public class InstancePacker {
    private Instance instance;
    private Runnable doneListener;

    public InstancePacker(Instance i) {
        this.instance = i;
    }

    public Runnable getDoneListener() {
        return doneListener;
    }

    public void setDoneListener(Runnable doneListener) {
        this.doneListener = doneListener;
    }

    public void start() {
        final WebProgressDialog progress = new WebProgressDialog(MainWindow.getInstance().getWindow(), "Packaging instance...");
        progress.setShowCloseButton(false);
        progress.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        progress.setLocationRelativeTo(MainWindow.getInstance().getWindow());
        progress.setModal(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        progress.setProgressText("Copying files...");
                        progress.setIndeterminate(true);
                    }
                });
                TFile targetFile = new TFile(instance.getRunningDir());
                File libraries = new File(targetFile, "libraries");
                FileUtils.clearDirectory(libraries);
                libraries.delete();
                TFile srcFile = new TFile(ConfigManager.getInstance().getVersionDir(instance.getVersion()), instance.getVersion().getId() + ".zip");
                try {
                    srcFile.cp_rp(targetFile);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        TVFS.umount(srcFile);
                    } catch (FsSyncException e) {
                        e.printStackTrace();
                    }
                }
                TFile jarFile = null;
                try {
                    jarFile = new TFile(targetFile, instance.getVersion().getId() + ".jar");
                    for (Mod m : instance.getMods()) {
                        if (m.getModType() != ModType.JAR) {
                            continue;
                        }
                        TFile modFile = new TFile(m.getFile());
                        modFile.cp_rp(jarFile);
                        TVFS.umount(modFile);
                    }
                    if (instance.shouldDeleteMetaInf()) {
                        new TFile(jarFile, "META-INF").rm_r();
                    }
                    FileUtils.clearDirectory(new File(targetFile, "mods"));
                    for (Mod m : instance.getMods()) {
                        if (m.getModType() != ModType.MODS) {
                            continue;
                        }
                        File targetModFile = new File(targetFile, "mods/" + m.getFile().getName());
                        TFile.cp_p(m.getFile(), targetModFile);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        TVFS.umount(targetFile);
                        TVFS.umount(jarFile);
                    } catch (FsSyncException e) {
                        e.printStackTrace();
                    }
                }
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        progress.dispose();
                        instance.setDirty(false);
                        InstanceManager.getInstance().save(instance);
                        doneListener.run();
                    }
                });
            }
        }, "JAR creator thread").start();
        progress.setVisible(true);
    }
}

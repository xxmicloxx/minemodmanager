package com.xxmicloxx.modmanager;

import com.alee.laf.WebLookAndFeel;
import com.xxmicloxx.modmanager.dialogs.InitialConfigurationDialog;
import de.schlichtherle.truezip.fs.spi.FsDriverService;

/**
 * @author miclo, mewking
 */
public class ModManager {

    private static ModManager ourInstance = new ModManager();

    public static ModManager getInstance() {
        return ourInstance;
    }

    private ModManager() {}

    public static void main(String[] args) {
        getInstance().init();
    }

    private void init() {
        WebLookAndFeel.install();
        WebLookAndFeel.setDecorateAllWindows(true);
        if (!Util.getConfigDir().exists() || ConfigManager.getInstance().notConfigExists()) {
            InitialConfigurationDialog dialog = new InitialConfigurationDialog();
            dialog.setTitle("MineModManager Configuration");
            dialog.pack();
            dialog.setVisible(true);
        }
        ConfigManager.getInstance().load();
        ConfigManager.getInstance().validateConfig();
        MainWindow window = MainWindow.getInstance();
        window.show();
    }
}

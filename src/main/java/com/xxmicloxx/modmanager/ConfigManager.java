package com.xxmicloxx.modmanager;

import com.google.gson.Gson;
import com.xxmicloxx.modmanager.gson.GsonSingleton;
import com.xxmicloxx.modmanager.i18n.I18n;
import com.xxmicloxx.modmanager.instances.Instance;
import com.xxmicloxx.modmanager.versions.Version;
import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * @author miclo, mewking
 */
public class ConfigManager {
    private Document config;
    private String minecraftDir;
    private String language;
    private int parallelDownloads = 5;
    private String username;
    private String encPassword;
    private UUID clientToken;

    private static ConfigManager instance;

    public static ConfigManager getInstance() {
        if (instance == null) {
            instance = new ConfigManager();
        }
        return instance;
    }

    public void validateConfig() {
        if (minecraftDir == null) {
            minecraftDir = Util.getDefaultMinecraftHome().getAbsolutePath();
        }
        if (language == null) {
            language = "us";
        }
        if (parallelDownloads <= 0) {
            parallelDownloads = 5;
        }
        if (clientToken == null) {
            clientToken = UUID.randomUUID();
        }
        save();
    }

    public void load() {
        File f = new File(Util.getConfigDir(), "config.xml");
        if (!f.exists()) {
            validateConfig();
        }
        SAXBuilder builder = new SAXBuilder();
        try {
            config = builder.build(f);
            Element root = config.getRootElement();
            List<Content> contentList = root.getContent();
            for (Content c : contentList) {
                if (c instanceof Element) {
                    Element e = (Element) c;
                    if (e.getName().equals("minecraftPath")) {
                        minecraftDir = e.getText();
                    } else if (e.getName().equals("language")) {
                        language = e.getText();
                    } else if (e.getName().equals("parallelDownloads")) {
                        parallelDownloads = Integer.parseInt(e.getText());
                    } else if (e.getName().equals("username")) {
                        username = e.getText();
                    } else if (e.getName().equals("password")) {
                        encPassword = e.getText();
                    } else if (e.getName().equals("clientToken")) {
                        clientToken = UUID.fromString(e.getText());
                    }
                }
            }
        } catch (JDOMException e) {
            //ignore this, as it can be a user error. in this case, just create a new valid config.
            validateConfig();
        } catch (IOException e) {
            e.printStackTrace();
        }
        I18n.i18n().reloadLanguage(this.language);
    }

    private void save() {
        Element rootElement = new Element("configuration");
        Element minecraftUri = new Element("minecraftPath");
        Element language = new Element("language");
        Element parallelDownloads = new Element("parallelDownloads");
        Element clientToken = new Element("clientToken");
        minecraftUri.setText(minecraftDir);
        language.setText(this.language);
        parallelDownloads.setText(String.valueOf(this.parallelDownloads));
        clientToken.setText(this.clientToken.toString());
        if (username != null) {
            Element username = new Element("username");
            username.setText(this.username);
            rootElement.addContent(username);
        }
        if (encPassword != null) {
            Element password = new Element("password");
            password.setText(this.encPassword);
            rootElement.addContent(password);
        }
        rootElement.addContent(minecraftUri);
        rootElement.addContent(language);
        rootElement.addContent(parallelDownloads);
        rootElement.addContent(clientToken);
        config = new Document(rootElement);
        File f = new File(Util.getConfigDir(), "config.xml");
        try {
            f.getParentFile().mkdirs();
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        XMLOutputter xmlOutput = new XMLOutputter();
        xmlOutput.setFormat(Format.getPrettyFormat());
        try {
            xmlOutput.output(config, new FileWriter(f));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getMinecraftDir() {
        return minecraftDir;
    }

    public File getMinecraftFile() {
        return new File(minecraftDir);
    }

    public File getVersionsDir() {
        File file = new File(getMinecraftFile(), "versions");
        file.mkdirs();
        return file;
    }

    public File getVersionDir(Version version) {
        File file = new File(getVersionsDir(), version.getId());
        file.mkdirs();
        File jsonFile = new File(file, version.getId() + ".json");
        Gson gson = GsonSingleton.getInstance();
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(jsonFile));
            bw.write(gson.toJson(version));
            bw.flush();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public void setMinecraftDir(String minecraftDir) {
        this.minecraftDir = minecraftDir;
        validateConfig();
    }

    public void setLanguage(String language) {
        if (language.equalsIgnoreCase("deutsch/german")) {
            language = "de";
        } else if (language.equalsIgnoreCase("english/english")) {
            language = "us";
        } else {
            language = "us";
        }
        this.language = language;
        validateConfig();
        I18n.i18n().reloadLanguage(this.language);
    }

    public UUID getClientToken() {
        return clientToken;
    }

    public void setClientToken(UUID clientToken) {
        this.clientToken = clientToken;
    }

    public String getLanguage() {
        return language;
    }

    public boolean notConfigExists() {
        return !new File(Util.getConfigDir(), "config.xml").exists();
    }

    public File getLibraryCache() {
        File libraryCache = new File(getMinecraftFile(), "libraryCache");
        libraryCache.mkdirs();
        return libraryCache;
    }

    public File getResourcesDir() {
        File resourcesDir = new File(getMinecraftFile(), "resources");
        resourcesDir.mkdirs();
        return resourcesDir;
    }

    public File getInstanceDir() {
        File instanceDir = new File(getMinecraftFile(), "instances");
        instanceDir.mkdirs();
        return instanceDir;
    }

    public int getParallelDownloads() {
        return parallelDownloads;
    }

    public void setParallelDownloads(int parallelDownloads) {
        this.parallelDownloads = parallelDownloads;
        validateConfig();
    }

    public File getModsDir() {
        File modsDir = new File(getMinecraftFile(), "mods");
        modsDir.mkdirs();
        return modsDir;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEncPassword() {
        return encPassword;
    }

    public void setEncPassword(String encPassword) {
        this.encPassword = encPassword;
    }

    public File getNativesDir() {
        File nativesDir = new File(getMinecraftFile(), "natives");
        nativesDir.mkdirs();
        return nativesDir;
    }

    public File getJarModsDir() {
        File modDir = new File(getModsDir(), "jar");
        modDir.mkdirs();
        return modDir;
    }

    public File getModsModsDir() {
        File modDir = new File(getModsDir(), "mods");
        modDir.mkdirs();
        return modDir;
    }
}

package com.xxmicloxx.modmanager.yggdrasil;

import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: Mic
 * Date: 13.11.13
 * Time: 20:43
 * To change this template use File | Settings | File Templates.
 */
public abstract class YggdrasilRequest {
    private UUID clientToken;
    private String accessToken;

    public void setClientToken(UUID clientToken) {
        this.clientToken = clientToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public abstract String getEndpoint();
}

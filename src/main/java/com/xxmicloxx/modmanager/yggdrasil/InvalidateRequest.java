package com.xxmicloxx.modmanager.yggdrasil;

/**
 * Created with IntelliJ IDEA.
 * User: Mic
 * Date: 17.11.13
 * Time: 19:56
 */
public class InvalidateRequest extends YggdrasilRequest {
    @Override
    public String getEndpoint() {
        return "invalidate";
    }
}

package com.xxmicloxx.modmanager.yggdrasil;

/**
 * Created with IntelliJ IDEA.
 * User: Mic
 * Date: 17.11.13
 * Time: 20:01
 */
public class AuthenticationException extends RuntimeException {
    private String errorMessage;

    public AuthenticationException(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}

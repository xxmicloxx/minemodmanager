package com.xxmicloxx.modmanager.yggdrasil;

/**
 * Created with IntelliJ IDEA.
 * User: Mic
 * Date: 16.11.13
 * Time: 21:32
 */
public class YggdrasilResult {
    private String error;
    private String errorMessage;
    private String cause;

    public String getError() {
        return error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getCause() {
        return cause;
    }
}

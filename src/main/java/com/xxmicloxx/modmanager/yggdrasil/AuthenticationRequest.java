package com.xxmicloxx.modmanager.yggdrasil;

/**
 * Created with IntelliJ IDEA.
 * User: Mic
 * Date: 16.11.13
 * Time: 23:10
 */
public class AuthenticationRequest extends YggdrasilRequest {
    private Agent agent = new Agent();

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getEndpoint() {
        return "authenticate";
    }

    private class Agent {
        private String name = "Minecraft";
        private int version = 1;
    }
}

package com.xxmicloxx.modmanager.yggdrasil;

/**
 * Created with IntelliJ IDEA.
 * User: Mic
 * Date: 17.11.13
 * Time: 19:52
 */
public class RefreshResult extends YggdrasilResult {
    private String accessToken;

    private AuthenticationResult.Profile selectedProfile;

    public String getAccessToken() {
        return accessToken;
    }

    public AuthenticationResult.Profile getSelectedProfile() {
        return selectedProfile;
    }
}

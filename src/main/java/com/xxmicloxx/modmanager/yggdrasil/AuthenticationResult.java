package com.xxmicloxx.modmanager.yggdrasil;

/**
 * Created with IntelliJ IDEA.
 * User: Mic
 * Date: 16.11.13
 * Time: 23:20
 */
public class AuthenticationResult extends YggdrasilResult {
    private String accessToken;
    private Profile[] availableProfiles;
    private Profile selectedProfile;

    public String getAccessToken() {
        return accessToken;
    }

    public Profile[] getAvailableProfiles() {
        return availableProfiles;
    }

    public Profile getSelectedProfile() {
        return selectedProfile;
    }

    public class Profile {
        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }
}

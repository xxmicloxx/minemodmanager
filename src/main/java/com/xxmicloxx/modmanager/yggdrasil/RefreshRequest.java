package com.xxmicloxx.modmanager.yggdrasil;

/**
 * Created with IntelliJ IDEA.
 * User: Mic
 * Date: 17.11.13
 * Time: 19:51
 */
public class RefreshRequest extends YggdrasilRequest {
    @Override
    public String getEndpoint() {
        return "refresh";
    }
}

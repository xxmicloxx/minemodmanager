package com.xxmicloxx.modmanager.yggdrasil;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.xxmicloxx.modmanager.ConfigManager;
import com.xxmicloxx.modmanager.gson.GsonSingleton;
import com.xxmicloxx.modmanager.network.HttpClientSingleton;
import org.apache.commons.io.IOUtils;

import javax.net.ssl.HttpsURLConnection;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

/**
 * Created with IntelliJ IDEA.
 * User: Mic
 * Date: 16.11.13
 * Time: 20:56
 */
public class Yggdrasil {
    private static Yggdrasil instance;
    public static Yggdrasil getInstance() {
        if (instance == null) {
            instance = new Yggdrasil();
        }
        return instance;
    }

    private AuthenticationResult.Profile lastProfile;

    public AuthenticationResult.Profile getLastProfile() {
        return lastProfile;
    }

    private <T extends YggdrasilResult> Future<T> request(final YggdrasilRequest request, final Class<T> cls) {
        return HttpClientSingleton.executorService.submit(new Callable<T>() {
            @Override
            public T call() throws Exception {
                try {
                    URL targetUrl = new URL("https://authserver.mojang.com/" + request.getEndpoint());
                    HttpsURLConnection conn = HttpClientSingleton.getConnection(targetUrl);
                    conn.setDoOutput(true);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json");
                    Gson gson = GsonSingleton.getInstance();
                    IOUtils.write(gson.toJson(request), conn.getOutputStream(), "UTF-8");
                    InputStream inputStream = null;
                    if (conn.getResponseCode() != 200) {
                        inputStream = conn.getErrorStream();
                    } else {
                        inputStream = conn.getInputStream();
                    }
                    String result = IOUtils.toString(inputStream, "UTF-8");
                    return gson.fromJson(result, cls);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });
    }

    public Future<String> authenticate(final String username, final String password) {
        return HttpClientSingleton.executorService.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                AuthenticationRequest request = new AuthenticationRequest();
                request.setUsername(username);
                request.setPassword(password);
                request.setClientToken(ConfigManager.getInstance().getClientToken());
                AuthenticationResult result = Yggdrasil.this.<AuthenticationResult>request(request, AuthenticationResult.class).get();
                if (result.getError() != null) {
                    throw new AuthenticationException(result.getErrorMessage());
                }
                lastProfile = result.getSelectedProfile();
                return result.getAccessToken();
            }
        });
    }

    public Future<String> refresh() {
        return HttpClientSingleton.executorService.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                RefreshRequest request = new RefreshRequest();
                request.setClientToken(ConfigManager.getInstance().getClientToken());
                request.setAccessToken(ConfigManager.getInstance().getEncPassword());
                RefreshResult result = Yggdrasil.this.<RefreshResult>request(request, RefreshResult.class).get();
                if (result.getError() != null) {
                    System.out.println("Yggdrasil result: " + GsonSingleton.getInstance().toJson(result));
                    throw new RuntimeException("Error while refreshing yggdrasil data.");
                }
                lastProfile = result.getSelectedProfile();
                return result.getAccessToken();
            }
        });
    }

    public Future<Object> invalidate() {
        return HttpClientSingleton.executorService.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                InvalidateRequest request = new InvalidateRequest();
                request.setAccessToken(ConfigManager.getInstance().getEncPassword());
                request.setClientToken(ConfigManager.getInstance().getClientToken());
                YggdrasilResult result = Yggdrasil.this.<YggdrasilResult>request(request, YggdrasilResult.class).get();
                if (result != null && result.getError() != null) {
                    throw new RuntimeException("Error while invalidating yggdrasil data.");
                }
                return null;
            }
        });
    }
}

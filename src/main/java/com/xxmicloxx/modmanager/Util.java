package com.xxmicloxx.modmanager;

import java.io.File;

/**
 * @author miclo, mewking
 */
public class Util {

    public static File getConfigDir() {
        return new File(new File(System.getProperty("user.home")), ".modmanager");
    }

    public static File getDefaultMinecraftHome() {
        return new File(new File(System.getProperty("user.home")), ".modmanagermc");
    }

    public static String getMirrorUrl() {
        return "http://178.63.221.188/modmanager/";
    }
}

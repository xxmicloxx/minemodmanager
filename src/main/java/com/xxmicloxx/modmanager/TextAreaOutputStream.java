package com.xxmicloxx.modmanager;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;

import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class TextAreaOutputStream extends OutputStream {

    private final JTextArea textArea;
    private final StringBuilder sb = new StringBuilder();
    private final PrintStream originalOut;

    public TextAreaOutputStream(final JTextArea textArea) {
        this.textArea = textArea;
        this.originalOut = System.out;
    }

    @Override
    public void flush() {
    }

    @Override
    public void close() {
    }

    @Override
    public void write(int b) throws IOException {

        if (b == '\r')
            return;

        if (b == '\n') {
            final String text = sb.toString() + "\n";
            originalOut.print(text);
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    textArea.append(text);
                    textArea.setCaretPosition(textArea.getDocument().getLength());
                }
            });
            sb.setLength(0);

            return;
        }

        sb.append((char) b);
    }
}
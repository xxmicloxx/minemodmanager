package com.xxmicloxx.modmanager.dialogs;

import com.alee.extended.panel.GroupPanel;
import com.alee.laf.list.WebList;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.rootpane.WebDialog;
import com.alee.laf.scroll.WebScrollPane;
import com.xxmicloxx.modmanager.i18n.I18n;
import com.xxmicloxx.modmanager.versions.LocalVersionManager;
import com.xxmicloxx.modmanager.versions.Version;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * @author miclo
 */
public class ChooseVersionDialog extends WebDialog {

    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private GroupPanel groupPanel;
    private WebScrollPane webScrollPane;
    private WebList lstVersions;
    private Version selectedVersion;

    private void createUIComponents() {
        groupPanel = new GroupPanel(webScrollPane = new WebScrollPane(lstVersions = new WebList()));
    }

    public ChooseVersionDialog(Version[] versions, Dialog parent) {
        super(parent);
        setContentPane(contentPane);
        setTitle(I18n.i18n().get("versions.download.choose"));
        setModal(true);
        pack();
        setLocationRelativeTo(null);
        getRootPane().setDefaultButton(buttonOK);
        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });
        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });
        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        //noinspection unchecked
        lstVersions.setListData(versions);
        lstVersions.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        lstVersions.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                if (mouseEvent.getButton() != 1) {
                    return;
                }
                if (mouseEvent.getClickCount() % 2 == 0) {
                    buttonOK.doClick();
                }
            }
        });
    }

    private void onOK() {
        if (lstVersions.getSelectedIndex() == -1) {
            WebOptionPane.showMessageDialog(this, I18n.i18n().get("versions.download.error.noselection"), I18n.i18n().get("generic.error"), JOptionPane.WARNING_MESSAGE);
            return;
        }
        if (LocalVersionManager.getInstance().getVersionById(((Version) lstVersions.getSelectedValue()).getId()) != null) {
            WebOptionPane.showMessageDialog(this, "This version was already downloaded.", "Error", JOptionPane.WARNING_MESSAGE);
            return;
        }
        selectedVersion = (Version) lstVersions.getSelectedValue();
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    public Version getSelectedVersion() {
        return selectedVersion;
    }
}

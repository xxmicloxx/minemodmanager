package com.xxmicloxx.modmanager.dialogs;

import com.alee.laf.rootpane.WebDialog;
import com.xxmicloxx.modmanager.i18n.I18n;
import com.xxmicloxx.modmanager.versions.Downloadable;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: ml
 * Date: 11.10.13
 * Time: 21:38
 */
public class LibraryDownloadDialog {
    private JPanel mainPane;
    private JList<Downloadable> mainList;

    private List<LibraryDownloadListener> listeners = new ArrayList<LibraryDownloadListener>();

    public void addLibraryDownloadListener(LibraryDownloadListener listener) {
        listeners.add(listener);
    }

    public LibraryDownloadDialog(final Set<Downloadable> downloadables, Dialog owner) {
        mainList.setCellRenderer(new LibraryDownloadCellRenderer());
        final DefaultListModel<Downloadable> dlm = new DefaultListModel<Downloadable>();
        for (Downloadable d : downloadables) {
            dlm.addElement(d);
        }
        mainList.setModel(dlm);

        final WebDialog frame = new WebDialog(owner, I18n.i18n().get("library.download.title"));
        frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        frame.setShowCloseButton(false);
        frame.add(mainPane);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);


        final Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            mainList.validate();
                            mainList.repaint();
                            for (Downloadable d : downloadables) {
                                if (d.getProgress() >= 100) {
                                    dlm.removeElement(d);
                                }
                            }
                            if (dlm.size() == 0) {
                                for (LibraryDownloadListener listener : listeners) {
                                    listener.onDownloadFinished();
                                }
                                frame.dispose();
                            }
                        }
                    });
                    boolean finish = true;
                    for (Downloadable d : downloadables) {
                        if (d.getProgress() != 100) {
                            finish = false;
                            break;
                        }
                    }
                    if (!frame.isVisible()) {
                        break;
                    }
                    if (finish) {
                        break;
                    }
                }
            }
        }, "Refresh thread");
        t.setDaemon(true);
        t.start();
    }

    public interface LibraryDownloadListener {
        public void onDownloadFinished();
    }
}

package com.xxmicloxx.modmanager.dialogs;

import com.alee.extended.filechooser.WebDirectoryChooser;
import com.alee.laf.rootpane.WebDialog;
import com.xxmicloxx.modmanager.ConfigManager;
import com.xxmicloxx.modmanager.Util;

import javax.swing.*;
import java.awt.event.*;
import java.io.File;

/**
 * @author miclo, mewking
 */
public class InitialConfigurationDialog extends WebDialog {

    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtMinecraftRoot;
    private JButton btnPathEdit;
    private JComboBox<String> comboBox1;

    public InitialConfigurationDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        comboBox1.removeAllItems();
        comboBox1.addItem("English/English");
        comboBox1.addItem("Deutsch/German");
        txtMinecraftRoot.setText(Util.getDefaultMinecraftHome().getAbsolutePath());
        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });
        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        btnPathEdit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                WebDirectoryChooser chooser = new WebDirectoryChooser(InitialConfigurationDialog.this);
                chooser.setVisible(true);
                if (chooser.getResult() == JFileChooser.APPROVE_OPTION) {
                    txtMinecraftRoot.setText(chooser.getSelectedDirectory().getAbsolutePath());
                }
            }
        });
    }

    private void onOK() {
        File file = new File(txtMinecraftRoot.getText());
        if (file.exists() && file.listFiles() != null && file.listFiles().length != 0) {
            JOptionPane.showMessageDialog(this, "The selected directory does already exist!");
            return;
        }
        file.mkdirs();
        ConfigManager.getInstance().setMinecraftDir(txtMinecraftRoot.getText());
        String lang = comboBox1.getItemAt(comboBox1.getSelectedIndex()).toLowerCase();
        ConfigManager.getInstance().setLanguage(lang);
        dispose();
    }

    private void onCancel() {
        dispose();
        System.exit(0);
    }
}

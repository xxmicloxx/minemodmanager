package com.xxmicloxx.modmanager.dialogs;

import com.alee.laf.list.WebListStyle;
import com.alee.laf.list.WebListUI;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.progressbar.WebProgressBar;
import com.xxmicloxx.modmanager.i18n.I18n;
import com.xxmicloxx.modmanager.versions.Downloadable;

import javax.swing.*;
import javax.swing.plaf.ListUI;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: ml
 * Date: 11.10.13
 * Time: 21:46
 */
public class LibraryDownloadCellRenderer extends WebPanel implements ListCellRenderer<Downloadable> {
    private WebProgressBar downloadBar;
    private JLabel fileNameLabel;

    public LibraryDownloadCellRenderer() {
        super();
        setOpaque(false);
        setName("List.cellRenderer");
        setLayout(new BorderLayout(0, 5));

        fileNameLabel = new JLabel();
        downloadBar = new WebProgressBar();
        add(fileNameLabel, BorderLayout.NORTH);
        add(downloadBar, BorderLayout.SOUTH);
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends Downloadable> list, Downloadable value, int index, boolean isSelected, boolean cellHasFocus) {
        setFont(list.getFont());
        setEnabled(list.isEnabled());
        setForeground(isSelected ? list.getSelectionForeground() : list.getForeground());

        fileNameLabel.setText(value.getLocal().getName());
        downloadBar.setValue((int) value.getProgress());

        if (value.getProgress() == 0) {
            downloadBar.setIndeterminate(true);
            downloadBar.setString(I18n.i18n().get("library.download.waiting"));
            downloadBar.setStringPainted(true);
        } else {
            downloadBar.setIndeterminate(false);
            downloadBar.setString(null);
        }

        //Border
        ListUI lui = list.getUI();
        int sw = lui instanceof WebListUI ? ((WebListUI) lui).getSelectionShadeWidth() : WebListStyle.selectionShadeWidth;
        setMargin(sw + 2, sw + 4, sw + 2, sw + 4);

        setComponentOrientation(list.getComponentOrientation());

        return this;
    }
}

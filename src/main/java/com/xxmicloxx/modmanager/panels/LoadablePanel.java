package com.xxmicloxx.modmanager.panels;

import com.alee.extended.transition.ComponentTransition;
import com.alee.extended.transition.TransitionListener;
import com.alee.extended.transition.effects.Direction;
import com.alee.extended.transition.effects.curtain.CurtainTransitionEffect;
import com.alee.extended.transition.effects.curtain.CurtainType;
import com.alee.extended.transition.effects.fade.FadeTransitionEffect;
import com.xxmicloxx.modmanager.panels.tabs.ITab;

import javax.swing.*;
import java.awt.*;

/**
 * @author miclo
 */
public class LoadablePanel extends JPanel {

    private boolean loading = false;
    private ComponentTransition transition;
    private JPanel content;
    private ITab tab;

    public LoadablePanel() {
        super(new BorderLayout());
    }

    public boolean isLoading() {
        return loading;
    }

    public ITab getTab() {
        return tab;
    }

    public void setTab(ITab tab) {
        this.tab = tab;
        setContent(tab.getRootPanel());
    }

    private void setContent(JPanel content) {
        if (transition == null) {
            transition = new ComponentTransition(content);
            FadeTransitionEffect effect = new FadeTransitionEffect();
            transition.setTransitionEffect(effect);
            add(transition, BorderLayout.CENTER);
        } else {
            if (transition.getContent() == this.content) {
                transition.delayTransition(0, content);
            }
        }
        this.content = content;
    }

    public void setLoading(boolean loading, final Runnable r) {
        this.loading = loading;
        if (loading) {
            LoadingPanel loadingPanel;
            transition.delayTransition(0, (loadingPanel = new LoadingPanel()).mainPanel);
            transition.addTransitionListener(new TransitionListener()  {
                @Override
                public void transitionStarted() {}

                @Override
                public void transitionFinished() {
                    transition.removeTransitionListener(this);
                    r.run();
                }
            });
        } else {
            transition.delayTransition(0, content);
            transition.addTransitionListener(new TransitionListener() {
                @Override
                public void transitionStarted() {}

                @Override
                public void transitionFinished() {
                    transition.removeTransitionListener(this);
                    r.run();
                }
            });
        }
    }

    private JPanel getContent() {
        return content;
    }
}

package com.xxmicloxx.modmanager.panels;

import com.alee.extended.panel.GroupPanel;
import com.xxmicloxx.modmanager.instances.Instance;
import com.xxmicloxx.modmanager.instances.InstanceManager;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Mic
 * Date: 23.11.13
 * Time: 20:18
 */
public class BuildSettingsPanel {
    private JPanel mainPanel;
    private JCheckBox chkDeleteMetaInf;

    private Instance instance;

    public BuildSettingsPanel() {
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public void setInstance(final Instance instance) {
        this.instance = instance;
        chkDeleteMetaInf.setSelected(instance.shouldDeleteMetaInf());
        chkDeleteMetaInf.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                instance.setShouldDeleteMetaInf(chkDeleteMetaInf.isSelected());
                instance.setDirty();
                InstanceManager.getInstance().save(instance);
            }
        });
    }
}

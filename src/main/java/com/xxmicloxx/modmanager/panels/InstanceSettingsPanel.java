package com.xxmicloxx.modmanager.panels;

import com.xxmicloxx.modmanager.instances.Instance;
import com.xxmicloxx.modmanager.instances.InstanceManager;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created with IntelliJ IDEA.
 * User: Mic
 * Date: 24.11.13
 * Time: 11:23
 */
public class InstanceSettingsPanel {
    private JPanel mainPanel;
    private JTextField txtAdditionalParameters;
    private JTextField txtCustomMain;
    private JTextField txtAdditionalVMParameters;

    private Instance instance;

    public InstanceSettingsPanel() {
        txtAdditionalParameters.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent keyEvent) {
                instance.setAdditionalParams(txtAdditionalParameters.getText());
                if (txtAdditionalParameters.getText().equals("")) {
                    instance.setAdditionalParams(null);
                }
                InstanceManager.getInstance().save(instance);
            }
        });
        txtAdditionalVMParameters.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent keyEvent) {
                instance.setAdditionalVMParameters(txtAdditionalVMParameters.getText());
                if (txtAdditionalVMParameters.getText().equals("")) {
                    instance.setAdditionalVMParameters(null);
                }
                InstanceManager.getInstance().save(instance);
            }
        });
        txtCustomMain.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent keyEvent) {
                instance.setMainClass(txtCustomMain.getText());
                if (txtCustomMain.getText().equals("")) {
                    instance.setMainClass(null);
                }
                InstanceManager.getInstance().save(instance);
            }
        });
    }

    public void setInstance(Instance instance) {
        this.instance = instance;
        txtAdditionalParameters.setText(instance.getAdditionalParams());
        txtCustomMain.setText(instance.getMainClass());
        txtAdditionalVMParameters.setText(instance.getAdditionalVMParameters());
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }
}

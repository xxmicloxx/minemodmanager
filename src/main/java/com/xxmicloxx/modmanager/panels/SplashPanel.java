package com.xxmicloxx.modmanager.panels;

import com.xxmicloxx.modmanager.i18n.I18n;

import javax.swing.*;

/**
 * @author miclo
 */
public class SplashPanel {
    public JPanel mainPanel;
    private JLabel copyRight;

    public SplashPanel() {
        copyRight.setText(I18n.i18n().get("manager.copyright"));
    }
}

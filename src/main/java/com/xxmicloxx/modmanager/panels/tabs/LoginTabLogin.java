package com.xxmicloxx.modmanager.panels.tabs;

import com.xxmicloxx.modmanager.MainWindow;
import com.xxmicloxx.modmanager.i18n.I18n;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author miclo
 */
public class LoginTabLogin {

    public JPanel mainPanel;
    private JTextField txtUsername;
    private JPasswordField txtPassword;
    private JButton btnRegister;
    private JButton btnLogin;
    private JLabel labelPW;
    private JLabel labelMail;

    public LoginTabLogin() {
        btnRegister.setText(I18n.i18n().get("accounts.register"));
        btnLogin.setText(I18n.i18n().get("accounts.login"));
        labelMail.setText(I18n.i18n().get("accounts.mail"));
        labelPW.setText(I18n.i18n().get("accounts.pw"));
        btnRegister.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ((LoginTab) MainWindow.getInstance().getTab(LoginTab.class)).setCurrentPanel(new LoginTabRegister().mainPanel);
            }
        });
    }
}

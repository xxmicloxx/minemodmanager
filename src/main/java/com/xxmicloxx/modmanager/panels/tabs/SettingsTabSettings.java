package com.xxmicloxx.modmanager.panels.tabs;

import com.alee.laf.optionpane.WebOptionPane;
import com.xxmicloxx.modmanager.ConfigManager;
import com.xxmicloxx.modmanager.MainWindow;
import com.xxmicloxx.modmanager.i18n.I18n;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author mewking
 */
public class SettingsTabSettings {

    public JPanel mainPanel;
    private JComboBox<String> comboBox1;
    private JButton button1;
    private JLabel label1;
    private JLabel lblParallelDownloads;
    private JSpinner txtParallelDownloads;

    public SettingsTabSettings() {
        comboBox1.removeAllItems();
        comboBox1.addItem("English/English");
        comboBox1.addItem("Deutsch/German");
        comboBox1.setSelectedIndex(I18n.i18n().getLanguageSelectorIndex());
        button1.setText(I18n.i18n().get("generic.save"));
        label1.setText(I18n.i18n().get("generic.language"));
        lblParallelDownloads.setText(I18n.i18n().get("config.paralleldownloads"));
        txtParallelDownloads.setModel(new SpinnerNumberModel(5, 1, 10, 1));
        txtParallelDownloads.setValue(ConfigManager.getInstance().getParallelDownloads());
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ConfigManager.getInstance().setParallelDownloads((Integer) txtParallelDownloads.getValue());
                ConfigManager.getInstance().setLanguage(comboBox1.getItemAt(comboBox1.getSelectedIndex()));

                if (WebOptionPane.showConfirmDialog(mainPanel, I18n.i18n().get("config.restart"), I18n.i18n().get("generic.confirm"),
                        WebOptionPane.YES_NO_OPTION, WebOptionPane.QUESTION_MESSAGE) != WebOptionPane.YES_OPTION) {
                    return;
                }
                try {
                    MainWindow.getInstance().restartApplication();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
    }
}

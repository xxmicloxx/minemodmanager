package com.xxmicloxx.modmanager.panels.tabs;

import com.alee.extended.panel.GroupPanel;
import com.alee.laf.StyleConstants;
import com.alee.laf.button.WebButton;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.table.WebTable;
import com.alee.laf.toolbar.ToolbarStyle;
import com.alee.laf.toolbar.WebToolBar;
import com.google.common.eventbus.Subscribe;
import com.xxmicloxx.modmanager.MainWindow;
import com.xxmicloxx.modmanager.events.EventManager;
import com.xxmicloxx.modmanager.events.instances.InstancesChangedEvent;
import com.xxmicloxx.modmanager.events.versions.LocalVersionsChangedEvent;
import com.xxmicloxx.modmanager.i18n.I18n;
import com.xxmicloxx.modmanager.icons.IconHelper;
import com.xxmicloxx.modmanager.instances.Instance;
import com.xxmicloxx.modmanager.instances.InstanceManager;
import com.xxmicloxx.modmanager.instances.InstanceStarter;
import com.xxmicloxx.modmanager.versions.LocalVersionManager;
import com.xxmicloxx.modmanager.versions.Version;
import com.xxmicloxx.modmanager.versions.VersionDownloader;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;

/**
 * Created with IntelliJ IDEA.
 * User: ml
 * Date: 11.10.13
 * Time: 15:43
 */
public class LocalVersionsTab implements ITab {
    private JPanel rootPane;
    private GroupPanel groupPanel1;
    private WebScrollPane webScrollPane1;
    private WebTable versionsTable;
    private WebToolBar mainToolBar;

    private WebButton trashButton;
    private WebButton testButton;

    public LocalVersionsTab() {
        EventManager.register(this);
    }

    private void createUIComponents() {
        groupPanel1 = new GroupPanel(webScrollPane1 = new WebScrollPane(versionsTable = new WebTable()), mainToolBar = new WebToolBar());
        versionsTable.setEditable(false);
        versionsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        webScrollPane1.setDrawBorder(false);
        groupPanel1.setUndecorated(true);
        groupPanel1.setDrawFocus(true);
        mainToolBar.setFloatable(false);
        mainToolBar.setToolbarStyle(ToolbarStyle.attached);
        versionsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (versionsTable.getSelectedRow() == -1) {
                    disableToolbar();
                } else {
                    enableToolbar();
                }
            }
        });
        initToolbar();
        refreshTable();
    }

    private void enableToolbar() {
        trashButton.setEnabled(true);
        testButton.setEnabled(true);
    }

    private void disableToolbar() {
        trashButton.setEnabled(false);
        testButton.setEnabled(false);
    }

    private void refreshTable() {
        DefaultTableModel dtm = new DefaultTableModel();
        dtm.addColumn(I18n.i18n().get("versions.table.version"));
        dtm.addColumn(I18n.i18n().get("versions.table.type"));
        dtm.addColumn(I18n.i18n().get("versions.table.release"));
        for (Version v : LocalVersionManager.getInstance().getVersions()) {
            dtm.addRow(new Object[]{v, v.getType(), v.getReleaseTime()});
        }
        versionsTable.setModel(dtm);
    }

    private void initToolbar() {
        WebButton downloadButton = WebButton.createIconWebButton(IconHelper.loadIcon("/com/xxmicloxx/modmanager/icons/download.png"), StyleConstants.smallRound, true);
        trashButton = WebButton.createIconWebButton(IconHelper.loadIcon("/com/xxmicloxx/modmanager/icons/trash.png"), StyleConstants.smallRound, true);
        testButton = WebButton.createIconWebButton(IconHelper.loadIcon("/com/xxmicloxx/modmanager/icons/test.png"), StyleConstants.smallRound, true);
        trashButton.setEnabled(false);
        testButton.setEnabled(false);
        downloadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                VersionDownloader.start();
            }
        });
        trashButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Version version = (Version) versionsTable.getModel().getValueAt(versionsTable.getSelectedRow(), 0);
                HashSet<Instance> instances = new HashSet<Instance>();
                for (Instance i : InstanceManager.getInstance().getInstances()) {
                    if (i.getVersion().getId().equals(version.getId())) {
                        //delete
                        instances.add(i);
                    }
                }
                if (instances.size() != 0) {
                    int result = WebOptionPane.showConfirmDialog(MainWindow.getInstance().getWindow(), "There are still instances of this version. They will get deleted.\nAre you really sure you want to continue?", "Warning", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                    if (result != WebOptionPane.YES_OPTION) {
                        return;
                    }
                }
                for (Instance i : instances) {
                    InstanceManager.getInstance().delete(i);
                }
                LocalVersionManager.getInstance().destroyVersion(version);
            }
        });
        testButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final Instance i = new Instance("test", (Version) versionsTable.getModel().getValueAt(versionsTable.getSelectedRow(), 0));
                InstanceManager.getInstance().getInstances().add(i);
                InstanceManager.getInstance().save(i);
                EventManager.post(new InstancesChangedEvent());
                InstanceStarter starter = new InstanceStarter(i);
                starter.setOnCompleteListener(new Runnable() {
                    @Override
                    public void run() {
                        InstanceManager.getInstance().delete(i);
                    }
                });
                starter.setOnCancelListener(new Runnable() {
                    @Override
                    public void run() {
                        InstanceManager.getInstance().delete(i);
                    }
                });
                starter.start();
            }
        });
        mainToolBar.addToEnd(downloadButton);
        mainToolBar.add(trashButton);
        mainToolBar.add(testButton);
    }

    @Override
    public String getTabTitle() {
        return I18n.i18n().get("versions.tab.title");
    }

    @Override
    public JPanel getRootPanel() {
        return rootPane;
    }

    @Override
    public void closing() {
    }

    @Subscribe
    public void onVersionsChanged(LocalVersionsChangedEvent ignored) {
        refreshTable();
    }
}

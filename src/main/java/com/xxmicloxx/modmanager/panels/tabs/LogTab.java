package com.xxmicloxx.modmanager.panels.tabs;

import com.alee.extended.panel.GroupPanel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.text.WebTextArea;
import com.xxmicloxx.modmanager.TextAreaOutputStream;

import javax.swing.*;
import java.io.PrintStream;

/**
 * Created with IntelliJ IDEA.
 * User: Mic
 * Date: 23.11.13
 * Time: 14:48
 */
public class LogTab implements ITab {
    private JPanel rootPane;
    private GroupPanel mainGroupPane;
    private WebScrollPane mainScrollPane;
    private WebTextArea txtLog;

    private void createUIComponents() {
        mainGroupPane = new GroupPanel(mainScrollPane = new WebScrollPane(txtLog = new WebTextArea()));
        mainGroupPane.setDrawFocus(false);
        mainScrollPane.setDrawBorder(false);
    }

    public LogTab() {
        TextAreaOutputStream taos = new TextAreaOutputStream(txtLog);
        PrintStream ps = new PrintStream(taos);

        System.setOut(ps);
        System.setErr(ps);
    }

    @Override
    public String getTabTitle() {
        return "Log";
    }

    @Override
    public JPanel getRootPanel() {
        return rootPane;
    }

    @Override
    public void closing() {
    }
}

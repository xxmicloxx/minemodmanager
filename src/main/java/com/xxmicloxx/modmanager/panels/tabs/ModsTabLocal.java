package com.xxmicloxx.modmanager.panels.tabs;

import com.alee.extended.panel.GroupPanel;
import com.alee.laf.StyleConstants;
import com.alee.laf.button.WebButton;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.table.WebTable;
import com.alee.laf.toolbar.ToolbarStyle;
import com.alee.laf.toolbar.WebToolBar;
import com.google.common.eventbus.Subscribe;
import com.xxmicloxx.modmanager.events.EventManager;
import com.xxmicloxx.modmanager.events.mods.LocalModsChangedEvent;
import com.xxmicloxx.modmanager.i18n.I18n;
import com.xxmicloxx.modmanager.icons.IconHelper;
import com.xxmicloxx.modmanager.mods.AssignModDialog;
import com.xxmicloxx.modmanager.mods.LocalModManager;
import com.xxmicloxx.modmanager.mods.Mod;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created with IntelliJ IDEA.
 * User: ml
 * Date: 25.10.13
 * Time: 22:07
 */
public class ModsTabLocal {
    public JPanel mainPanel;
    private GroupPanel groupPanel1;
    private WebScrollPane webScrollPane1;
    private WebToolBar toolbar;
    private WebTable table;
    private WebButton addToInstanceButton;

    public ModsTabLocal() {
        EventManager.register(this);
    }

    public void createUIComponents() {
        groupPanel1 = new GroupPanel();
        toolbar = new WebToolBar();
        toolbar.setToolbarStyle(ToolbarStyle.attached);
        toolbar.setFloatable(false);
        //TODO buttons
        WebButton refreshButton = WebButton.createIconWebButton(IconHelper.loadIcon("/com/xxmicloxx/modmanager/icons/refresh.png"), StyleConstants.smallRound, true);
        refreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LocalModManager.getInstance().refreshMods();
            }
        });
        toolbar.addToEnd(refreshButton);
        addToInstanceButton = WebButton.createIconWebButton(IconHelper.loadIcon("/com/xxmicloxx/modmanager/icons/add.png"), StyleConstants.smallRound, true);
        addToInstanceButton.setEnabled(false);
        addToInstanceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (table.getSelectedRowCount() == 1) {
                    Mod m = (Mod) table.getModel().getValueAt(table.getSelectedRow(), table.getSelectedColumn());
                    new AssignModDialog(m);
                }
            }
        });
        toolbar.add(addToInstanceButton);
        webScrollPane1 = new WebScrollPane(table = new WebTable(), false);
        groupPanel1.setDrawFocus(false);
        refreshTable();
        table.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (table.getSelectedRowCount() == 1) {
                    addToInstanceButton.setEnabled(true);
                } else {
                    addToInstanceButton.setEnabled(false);
                }

                if (e.getButton() != 1) {
                    return;
                }
                if (e.getClickCount() % 2 == 0 && addToInstanceButton.isEnabled()) {
                    addToInstanceButton.doClick();
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {}

            @Override
            public void mouseReleased(MouseEvent e) {}

            @Override
            public void mouseEntered(MouseEvent e) {}

            @Override
            public void mouseExited(MouseEvent e) {}
        });
    }

    @Subscribe
    public void onLocalModsRefresh(LocalModsChangedEvent e) {
        refreshTable();
    }

    private void refreshTable() {
        DefaultTableModel dtm = new DefaultTableModel();
        dtm.addColumn(I18n.i18n().get("mods.column"));
        dtm.addColumn("Type");
        for (Mod m : LocalModManager.getInstance().getMods()) {
            dtm.addRow(new Object[]{m, m.getModType()});
        }
        table.setEditable(false);
        table.setModel(dtm);
        addToInstanceButton.setEnabled(false);
    }
}

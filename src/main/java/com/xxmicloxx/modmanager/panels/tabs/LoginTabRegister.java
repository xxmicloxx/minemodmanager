package com.xxmicloxx.modmanager.panels.tabs;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.xxmicloxx.modmanager.MainWindow;
import com.xxmicloxx.modmanager.i18n.I18n;
import com.xxmicloxx.modmanager.network.RegisterHelper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author miclo
 */
public class LoginTabRegister {

    public JPanel mainPanel;
    private JTextField txtEmail;
    private JPasswordField txtPassword;
    private JTextField txtPasswordRepeat;
    private JTextField txtCaptcha;
    private JLabel lblCaptcha;
    private JComboBox cmbSecurityQuestion;
    private JTextField txtSecurityQuestion;
    private JButton btnCancel;
    private JButton btnRegister;
    private JLabel labelMail;
    private JLabel labelPW;
    private JLabel labelPW2;
    private JLabel labelHuman;
    private JLabel labelSecuQuest;
    private JLabel labelSecuAnswer;
    private int questionId;

    public LoginTabRegister() {
        btnCancel.setText(I18n.i18n().get("generic.cancel") + ":");
        btnRegister.setText(I18n.i18n().get("accounts.register") + ":");
        labelMail.setText(I18n.i18n().get("accounts.mailonly") + ":");
        labelPW.setText(I18n.i18n().get("accounts.pw") + ":");
        labelPW2.setText(I18n.i18n().get("accounts.pwrepeat") + ":");
        labelHuman.setText(I18n.i18n().get("accounts.humanveri") + ":");
        labelSecuQuest.setText(I18n.i18n().get("accounts.secuquest") + ":");
        labelSecuAnswer.setText(I18n.i18n().get("accounts.secuanswer") + ":");
        lblCaptcha.setText(I18n.i18n().get("generic.loading"));
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ((LoginTab) MainWindow.getInstance().getTab(LoginTab.class)).setCurrentPanel(new LoginTabLogin().mainPanel);
            }
        });
        Futures.addCallback(RegisterHelper.getRandomQuestion(), new FutureCallback<RegisterHelper.RandomQuestion>() {
            @Override
            public void onSuccess(final RegisterHelper.RandomQuestion randomQuestion) {
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        lblCaptcha.setText(randomQuestion.getQuestion());
                        questionId = randomQuestion.getId();
                    }
                });
            }

            @Override
            public void onFailure(Throwable throwable) {
                System.out.println("ERROR");
                throwable.printStackTrace();
            }
        });
    }
}

package com.xxmicloxx.modmanager.panels.tabs;

import com.xxmicloxx.modmanager.MainWindow;
import com.xxmicloxx.modmanager.i18n.I18n;

import javax.swing.*;

/**
 * @author miclo
 */
public class LoginTab implements ITab {

    private JPanel currentPanel;

    public LoginTab() {
        currentPanel = new LoginTabLogin().mainPanel;
    }

    @Override
    public String getTabTitle() {
        return I18n.i18n().get("accounts.login");
    }

    @Override
    public JPanel getRootPanel() {
        return currentPanel;
    }

    @Override
    public void closing() {
    }

    public JPanel getCurrentPanel() {
        return currentPanel;
    }

    public void setCurrentPanel(JPanel currentPanel) {
        this.currentPanel = currentPanel;
        MainWindow.getInstance().refreshContent(this);
    }
}

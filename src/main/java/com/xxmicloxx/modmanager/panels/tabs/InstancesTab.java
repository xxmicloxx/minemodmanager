package com.xxmicloxx.modmanager.panels.tabs;

import com.alee.extended.panel.GroupPanel;
import com.alee.laf.StyleConstants;
import com.alee.laf.button.WebButton;
import com.alee.laf.list.WebList;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.text.WebTextArea;
import com.alee.laf.toolbar.ToolbarStyle;
import com.alee.laf.toolbar.WebToolBar;
import com.alee.laf.tooltip.WebToolTip;
import com.alee.managers.popup.PopupWay;
import com.alee.managers.popup.WebButtonPopup;
import com.alee.managers.tooltip.TooltipManager;
import com.alee.managers.tooltip.TooltipWay;
import com.alee.managers.tooltip.WebCustomTooltip;
import com.google.common.eventbus.Subscribe;
import com.xxmicloxx.modmanager.MainWindow;
import com.xxmicloxx.modmanager.events.EventManager;
import com.xxmicloxx.modmanager.events.instances.InstancesChangedEvent;
import com.xxmicloxx.modmanager.i18n.I18n;
import com.xxmicloxx.modmanager.icons.IconHelper;
import com.xxmicloxx.modmanager.instances.*;
import com.xxmicloxx.modmanager.panels.BuildSettingsPanel;
import com.xxmicloxx.modmanager.panels.InstanceSettingsPanel;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;

/**
 * @author miclo
 */
public class InstancesTab implements ITab {

    private final BuildSettingsPanel buildSettingsPanel;
    private final InstanceSettingsPanel instanceSettingsPanel;
    private JPanel mainPanel;
    private JButton btnPlay;
    private JButton btnRename;
    private JButton btnCopy;
    private WebButton btnSettings;
    private JButton btnEdit;
    private JButton btnSaves;
    private JButton btnRebuild;
    private GroupPanel groupPanel1;
    private WebScrollPane webScrollPane1;
    private WebList lstInstances;
    private GroupPanel groupPanel2;
    private WebScrollPane webScrollPane2;
    private WebTextArea txtInfo;
    private WebToolBar lstToolBar;
    private JLabel label1;
    private WebButton btnBuildSettings;

    private void createUIComponents() {
        lstToolBar = new WebToolBar(WebToolBar.HORIZONTAL);
        groupPanel1 = new GroupPanel(webScrollPane1 = new WebScrollPane(lstInstances = new WebList()), lstToolBar);
        webScrollPane1.setDrawBorder(false);
        groupPanel1.setUndecorated(false);
        groupPanel1.setDrawFocus(true);
        groupPanel2 = new GroupPanel(webScrollPane2 = new WebScrollPane(txtInfo));
        lstToolBar.setFloatable(false);
        lstToolBar.setToolbarStyle(ToolbarStyle.attached);
        WebButton addWebButton = WebButton.createIconWebButton(IconHelper.loadIcon("/com/xxmicloxx/modmanager/icons/add.png"), StyleConstants.smallRound, true);
        final WebButton deleteWebButton = WebButton.createIconWebButton(IconHelper.loadIcon("/com/xxmicloxx/modmanager/icons/trash.png"), StyleConstants.smallRound, true);
        deleteWebButton.setEnabled(false);
        addWebButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new InstanceCreationDialog();
            }
        });
        deleteWebButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                InstanceManager.getInstance().delete((Instance) lstInstances.getSelectedValue());
            }
        });
        lstToolBar.addToEnd(addWebButton);
        lstToolBar.add(deleteWebButton);

        lstInstances.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (lstInstances.getSelectedIndex() == -1) {
                    deleteWebButton.setEnabled(false);
                    enableButtons(false);
                    updateComponents();
                } else {
                    deleteWebButton.setEnabled(true);
                    enableButtons(true);
                    updateComponents();
                }
            }
        });

        lstInstances.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                if (mouseEvent.getButton() != 1) {
                    return;
                }
                if (mouseEvent.getClickCount() % 2 == 0) {
                    btnPlay.doClick();
                }
            }
        });

        refreshInstances();

        EventManager.register(this);
    }

    private void updateComponents() {
        if (lstInstances.getSelectedIndex() == -1) {
            label1.setText("Select Instance");
            txtInfo.setText("");
            txtInfo.setEnabled(false);
        } else {
            Instance i = (Instance) lstInstances.getSelectedValue();
            label1.setText(i.getName());
            txtInfo.setText(i.getDescription());
            txtInfo.setEnabled(true);
            buildSettingsPanel.setInstance(i);
            instanceSettingsPanel.setInstance(i);
        }
    }

    public InstancesTab() {
        enableButtons(false);
        btnPlay.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new InstanceStarter((Instance) lstInstances.getSelectedValue()).start();
            }
        });
        txtInfo.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent keyEvent) {
                if (lstInstances.getSelectedIndex() == -1) {
                    return;
                }
                Instance i = (Instance) lstInstances.getSelectedValue();
                i.setDescription(txtInfo.getText());
                InstanceManager.getInstance().save(i);
            }
        });
        btnRebuild.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Instance i = (Instance) lstInstances.getSelectedValue();
                i.setDirty();
                InstanceManager.getInstance().save(i);
                TooltipManager.showOneTimeTooltip(btnRebuild, new Point(0, btnRebuild.getHeight() / 2),
                        "Instance marked for rebuild.", TooltipWay.left);
            }
        });

        btnEdit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Instance i = (Instance) lstInstances.getSelectedValue();
                EditModsDialog dialog = new EditModsDialog(i);
                dialog.setLocationRelativeTo(MainWindow.getInstance().getWindow());
                dialog.setVisible(true);
            }
        });

        WebButtonPopup buildSettingsPopup = new WebButtonPopup(btnBuildSettings, PopupWay.leftCenter);
        buildSettingsPanel = new BuildSettingsPanel();
        buildSettingsPopup.setContent(buildSettingsPanel.getMainPanel());

        WebButtonPopup instanceSettingsPopup = new WebButtonPopup(btnSettings, PopupWay.leftCenter);
        instanceSettingsPanel = new InstanceSettingsPanel();
        instanceSettingsPopup.setContent(instanceSettingsPanel.getMainPanel());
    }

    private void enableButtons(boolean b) {
        btnCopy.setEnabled(b);
        btnEdit.setEnabled(b);
        btnPlay.setEnabled(b);
        btnRebuild.setEnabled(b);
        btnRename.setEnabled(b);
        btnSaves.setEnabled(b);
        btnSettings.setEnabled(b);
        btnBuildSettings.setEnabled(b);
    }

    public void applyI18n() {
        btnPlay.setText(I18n.i18n().get("instances.play"));
        btnRename.setText(I18n.i18n().get("instances.rename"));
        btnCopy.setText(I18n.i18n().get("instances.copyinstance"));
        btnSettings.setText(I18n.i18n().get("instances.settings"));
        btnEdit.setText(I18n.i18n().get("instances.managemods"));
        btnSaves.setText(I18n.i18n().get("instances.managesaves"));
        btnRebuild.setText(I18n.i18n().get("instances.rebuild"));
    }

    @Subscribe
    public void onInstancesChanged(InstancesChangedEvent e) {
        refreshInstances();
    }

    private void refreshInstances() {
        DefaultListModel<Instance> dlm = new DefaultListModel<Instance>();
        for (Instance i : InstanceManager.getInstance().getInstances()) {
            dlm.addElement(i);
        }
        lstInstances.setEditable(false);
        //noinspection unchecked
        lstInstances.setModel(dlm);
    }

    @Override
    public String getTabTitle() {
        return I18n.i18n().get("instances.title");
    }

    @Override
    public JPanel getRootPanel() {
        return mainPanel;
    }

    @Override
    public void closing() {
    }
}

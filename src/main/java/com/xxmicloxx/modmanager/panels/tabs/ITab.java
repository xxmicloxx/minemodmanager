package com.xxmicloxx.modmanager.panels.tabs;

import javax.swing.*;

/**
 * @author miclo
 */
public interface ITab {

    public String getTabTitle();
    public JPanel getRootPanel();
    public void closing();
}

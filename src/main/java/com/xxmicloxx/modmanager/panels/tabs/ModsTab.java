package com.xxmicloxx.modmanager.panels.tabs;

import com.alee.extended.breadcrumb.WebBreadcrumb;
import com.alee.extended.breadcrumb.WebBreadcrumbToggleButton;
import com.alee.extended.panel.GroupPanel;
import com.alee.extended.statusbar.WebStatusBar;
import com.alee.laf.panel.WebPanel;
import com.xxmicloxx.modmanager.i18n.I18n;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: ml
 * Date: 25.10.13
 * Time: 20:20
 */
public class ModsTab implements ITab {
    private JPanel mainPanel;
    private GroupPanel groupPanel1;
    private WebStatusBar statusBar;
    private WebBreadcrumb breadcumb;
    private WebPanel contentPanel;
    private JPanel currentContent;

    public void createUIComponents() {
        groupPanel1 = new GroupPanel();
        contentPanel = new WebPanel(new BorderLayout());
        statusBar = new WebStatusBar();
        breadcumb = new WebBreadcrumb(true);
        groupPanel1.setDrawFocus(true);

        ButtonGroup buttonGroup = new ButtonGroup();
        WebBreadcrumbToggleButton localButton = new WebBreadcrumbToggleButton();
        localButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                contentPanel.remove(currentContent);
                contentPanel.add(currentContent = new ModsTabLocal().mainPanel, BorderLayout.CENTER);
                contentPanel.validate();
            }
        });
        localButton.setText(I18n.i18n().get("mods.local"));
        breadcumb.add(localButton);
        buttonGroup.add(localButton);

        WebBreadcrumbToggleButton remoteButton = new WebBreadcrumbToggleButton();
        remoteButton.setText(I18n.i18n().get("mods.remote"));
        remoteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                contentPanel.remove(currentContent);
                contentPanel.add(currentContent = new ModsTabRemote().mainPanel, BorderLayout.CENTER);
                contentPanel.validate();
            }
        });
        breadcumb.add(remoteButton);
        buttonGroup.add(remoteButton);
        localButton.setSelected(true);
        contentPanel.add(currentContent = new ModsTabLocal().mainPanel, BorderLayout.CENTER);
    }

    @Override
    public String getTabTitle() {
        return I18n.i18n().get("mods.title");
    }

    @Override
    public JPanel getRootPanel() {
        return mainPanel;
    }

    @Override
    public void closing() {
    }
}

package com.xxmicloxx.modmanager.panels.tabs;

import com.alee.extended.panel.GroupPanel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.text.WebTextArea;
import com.xxmicloxx.modmanager.MainWindow;
import com.xxmicloxx.modmanager.instances.Instance;
import com.xxmicloxx.modmanager.process.JavaProcess;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: ml
 * Date: 11.12.13
 * Time: 19:17
 */
public class InstanceWatcher implements ITab {
    private GroupPanel mainGroupPanel;
    private JPanel mainPanel;
    private WebScrollPane mainScrollPane;
    private WebTextArea mainTextArea;
    private JButton btnKill;
    private JLabel lblStatus;
    private JavaProcess process;

    public InstanceWatcher(final JavaProcess process) {
        this.process = process;
        btnKill.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                process.stop();
            }
        });
    }

    private void createUIComponents() {
        mainGroupPanel = new GroupPanel(mainScrollPane = new WebScrollPane(mainTextArea = new WebTextArea()));
    }

    @Override
    public String getTabTitle() {
        return "\"" + process.getInstance().getName() + "\"";
    }

    @Override
    public JPanel getRootPanel() {
        return mainPanel;
    }

    @Override
    public void closing() {
    }

    public void setProcessEnded(int exitCode) {
        lblStatus.setText("ended with exit code " + exitCode);
        btnKill.setEnabled(false);
        MainWindow.getInstance().setTabCloseable(this, true);
    }

    public void appendLogLine(String line) {
        mainTextArea.append(line);
        mainTextArea.append("\n");
    }
}
